c                        web server version
c
c     This version includes calculation of DDG fpr mutants. Limitations:
c     1. A single mutation in a single subunit.
c     2. If two different pdb files are used for the wild-type and mutant,
c     the numbering of residues in both files must be the same
c
c     Determination of pairwise TMH association energies and assembly
c     pathways for a number of 3D structures
c
c     Input: "subunits" file taken from OPM database. The corresponding
c     PDB files should be in the same directory
c
c     This is version for a set of proteins that belong to the same
c     (super)family. We assume the same number of TMH in all proteins from 
c     the set. "Extra" helices if any should be remouved from input
c     file. Input file includes DSSP-generated sequence, secondary 
c     structure and disorder records (subunits_last_inp).
c
c     Current version: (a) set of structures from a family; (b) use DSSP sequences; 
c     (c) output various segments and aa sequence (complete), 
c     (d) generate Table for proteis from the family.
c
      parameter (maxsub=12000,maxpair=100,maxstr=500,maxat=50000,
     *  maxhel=50,maxres=8000,maxrs=30)
c
      integer ib(100),num1(maxhel),num2(maxhel),numtm1(2),numtm2(2),
     *  indpair(maxpair),ibuf(2,maxpair),numat(maxat),
     *  nnum1(100,40),nnum2(100,40),nnhelix(100),
     *  numhout(2,10)/1,2,1,7,2,3,2,7,3,4,3,5,3,7,4,5,5,6,6,7/,
     *  numres(maxres),iprof(maxres)
      character*18000 line00
      character*1 subunit(100),subtable(maxstr),aresol
      character*4 pdb(maxsub),pdbtable(maxstr),namat(maxat),
     *  namres(maxat),resw,resm,respro(maxrs)
      character*5 ntmh(maxsub)
      character*6 uniprot
      character*500 tmh(maxsub),tmh0
      character*80 nametable(maxstr),line
      character*180 pdbtempl,pdbtempl2,filefmap
      character*180 pdbpath
      character*8 restable(maxstr)
      real etable(maxpair,maxstr),ebuf(maxpair),xyz(3,maxat),
     *  ematr(maxhel,maxhel),eij(50,50),eij2(50,50),prof(23,121)
c
c     open (7,file='test.inp')
      ecutoff=-1.0
      tmh0=' '
      mm=0
c     open(81,file='corrections_test.txt')
c     open(91,file='assembl_table2.txt')
c     open (23,file='table2')
c***  read (*,'(i1)') imut
      imut=3
c
c     Mutations only for a single subunit
c
      read (*,'(a)') pdbtempl
      iii=index(pdbtempl,' ')-1
      write (*,'(''PDB file: '',a)') pdbtempl(1:60)
      if(imut.eq.1) then
         ireturn=0
         read (*,'(a1,1x,a)',end=10) subunit(1),tmh(1)
         i00=index(tmh(1),'         ')-1
         m=0
c        write (*,'(i4,a)') numtmh,tmh(1)(1:i00)
         do k=1,i00-5
            if(k.eq.1.or.tmh(1)(k:k).eq.',') then
               m=m+1
               i01=index(tmh(1)(k:i00),'(')+1
               i02=index(tmh(1)(k:i00),'-')-1
               i03=index(tmh(1)(k:i00),'-')+1
               i04=index(tmh(1)(k:i00),')')-1
               read (tmh(1)(k+i01-1:k+i02-1),'(i5)') num1(m)
               read (tmh(1)(k+i03-1:k+i04-1),'(i5)') num2(m)
               nnum1(1,m)=num1(m)
               nnum2(1,m)=num2(m)
            end if
         end do
         write (*,'(''Subunit: '',a1)') subunit(1)
         write (*,'(''TM segments: '',60i4)') 
     *     (num1(l),num2(l),l=1,m)
         nhelix=m
         nnhelix(1)=nhelix
c
c        Read amino acid sequence of this subunit from the pdb coordinate
c        file and redefine boundaries of TM segments if needed. The 
c        numbers of residues are defined as in the PDB file. 
c
         call corbound (pdbtempl,subunit(1),nhelix,num1,num2,ireturn,
     *     nat,numat,namat,namres,xyz,iresol,uniprot)
c
         call bind(pdbtempl,subunit(1),subunit(1),
     *     numtm1,numtm2,eassoc,ireturn,2,nhelix,num1,num2,
     *     subunit,nnhelix,nnum1,nnum2,1,1,1)
c        write (*,'(f8.1)') eassoc
         do icycle=1,999
            read (*,'(a)',end=80) pdbtempl2
            read (*,'(a4,i4,1x,a4,f5.2)',end=80) resw,nmut,resm,dmut1
            do l=1,nhelix
               if(nmut.ge.num1(l).and.nmut.le.num2(l)) go to 2
            end do
            write (*,'(''mutation outside TM segments: '',
     *        a4,i4,1x,a4)') resw,nmut,resm
            go to 4
c
    2       continue
            if(pdbtempl2.eq.pdbtempl) call subst(resw,nmut,resm,
     *        pdbtempl,pdbtempl2)
c           write (*,'(''PDB file: '',a)') pdbtempl2(1:60)
            call bind(pdbtempl2,subunit(1),subunit(1),
     *        numtm1,numtm2,eassoc2,ireturn,2,nhelix,num1,num2,
     *        subunit,nnhelix,nnum1,nnum2,1,1,1)
c           write (*,'(f8.1)') eassoc2
            dmut=eassoc2-eassoc
c           write (*,'(3(a4,1x),i5,1x,a4,3f6.1)') pdbtempl(1:4),
c    *        pdbtempl2(1:4),resw,nmut,resm,eassoc,eassoc2,dmut
            write (*,'(3(a4,'';''),i5,'';'',a4,'';'',f6.1,'';'',f6.1)')
     *        pdbtempl(1:4),pdbtempl2(1:4),resw,nmut,resm,dmut1,dmut
    4       continue
         end do
   80    continue
         stop
      end if
c
      do i=1,maxsub
         ireturn=0
         read (*,'(a)',end=10) line00
         indcheck=index(line00,'.pdb')
         if(indcheck.ne.0) go to 10
         read (line00,'(a1,1x,a)') subunit(i),tmh(i)
         i00=index(tmh(i),'         ')-1
         m=0
c        write (*,'(i4,a)') numtmh,tmh(i)(1:i00)
         do k=1,i00-5
            if(k.eq.1.or.tmh(i)(k:k).eq.',') then
               m=m+1
               i01=index(tmh(i)(k:i00),'(')+1
               i02=index(tmh(i)(k:i00),'-')-1
               i03=index(tmh(i)(k:i00),'-')+1
               i04=index(tmh(i)(k:i00),')')-1
               read (tmh(i)(k+i01-1:k+i02-1),'(i5)') num1(m)
               read (tmh(i)(k+i03-1:k+i04-1),'(i5)') num2(m)
               nnum1(i,m)=num1(m)
               nnum2(i,m)=num2(m)
            end if
         end do
         write (*,'('' '')') 
         write (*,'(''Subunit: '',a1)') subunit(i)
         write (*,'(''TM segments: '',60i4)') 
     *     (num1(l),num2(l),l=1,m)
c        write (91,'(a4,''|'',a1,2(''|'',1x,a),''|'',a6,''|'')') 
c    *     pdb(i),subunit(i),line00(i4:j4),line00(i5:j5),uniprot
         nhelix=m
         nnhelix(i)=nhelix
         if(nhelix.gt.1) then
            do j=1,nhelix-1
               if(num2(j).ge.num1(j+1)+1) then
                  write (*,'(''There is intermediate residue between'',
     *              two adjacent TM segments:'',2(i5,''-'',i5))') 
     *              num1(j),num2(j),num1(j+1),num2(j+1)
                  write (*,'(''Please redefine TM segments'')')
                  stop
               end if
            end do
         end if
c
c        Read amino acid sequence of this subunit from the pdb coordinate
c        file and redefine boundaries of TM segments if needed. The 
c        numbers of residues are defined as in the PDB file. 
c
         call corbound (pdbtempl,subunit(i),nhelix,num1,num2,ireturn,
     *      nat,numat,namat,namres,xyz,iresol,uniprot)
c
c        go to 6
c***
c
c        write (*,'(2i3)') nhelix,ireturn
         if(nhelix.ge.2.and.ireturn.eq.0) then
            eastot=0.
            m0=0
            do j=1,nhelix-1
               ematr(j,j)=0.
               do k=j+1,nhelix
                  numtm1(1)=num1(j)
                  numtm2(1)=num2(j)
                  numtm1(2)=num1(k)
                  numtm2(2)=num2(k)
c
c                 association energy for pairs of TMH in the subunit:
c
                  call bind(pdbtempl,subunit(i),subunit(i),
     *              numtm1,numtm2,eassoc,ireturn,1,nhelix,num1,num2,
     *              subunit,nnhelix,nnum1,nnum2,1,1,1)
c                 eassoc=eassoc/1.65
                  if(ireturn.eq.0) then
                     m0=m0+1
                     ebuf(m0)=eassoc
                     ematr(j,k)=eassoc
                     ematr(k,j)=eassoc
                     eastot=eastot+eassoc
                     if(eassoc.lt.ecutoff) then 
                        write(*,'(''TMH'',i4,''-'',i4,f6.1,
     *                    '' kcal/mol'')')j,k,eassoc
                     end if
                  else
                     write (*,'(2i4)') j,k
                     go to 5
                  end if
               end do
            end do
c
c           association energy of all TMH in the subunit:
c
            call bind (pdbtempl,subunit(i),subunit(i),
     *        numtm1,numtm2,eassoc,ireturn,2,nhelix,num1,num2,
     *        subunit,nnhelix,nnum1,nnum2,1,1,1)
            write (*,'(''Energy of association of all TM '',
     *        ''helices in this subunit:'',f7.1,
     *        '' kcal/mol'')') eassoc
            mm=mm+1
            do l=1,m0
               etable(l,mm)=ebuf(l)
            end do
            pdbtable(mm)=pdbtempl(1:4)
            restable(mm)=line00(i4:j4)
            nametable(mm)=line00(i5:j5)
            subtable(mm)=subunit(i)
c
    5       continue
c
            iii=index(pdbtempl,'.')-1
            if(iii.le.0) iii=index(pdbtempl,'  ')-1
            pdbpath=pdbtempl(1:iii)//subunit(i)//'path.pdb'
            call pathway (nhelix,num1,num2,ematr,
     *        nat,numat,namat,namres,xyz,subunit(i),
     *        pdbpath)
         end if
c   6    continue
c***
      end do
   10 continue
c
      nnsub=i-1
c     write (*,'(i3)') nnsub
      if(nnsub.ge.2) then
         write(*,'('' '')')
         write(*,'(''Free energy of association of TM helices '',
     *     ''from different subunits:'')')
         ediss=0.
         do i1=1,nnsub-1
            do j1=i1+1,nnsub
               eij(i1,j1)=0.
               eij2(i1,j1)=0.
               do i=1,nnhelix(i1)
                  do j=1,nnhelix(j1)
                     numtm1(1)=nnum1(i1,i)
                     numtm2(1)=nnum2(i1,i)
                     numtm1(2)=nnum1(j1,j)
                     numtm2(2)=nnum2(j1,j)
c
c                    association energy for pairs of TMH that belong to 
c                    different subunits:
c
c                    write(*,'(2(''subunit '', 
c    *                 a1,'' helix'',i4,'','',2x))')
c    *                 subunit(i1),i,subunit(j1),j
                     call bind(pdbtempl,subunit(i1),subunit(j1),
     *                 numtm1,numtm2,eassoc,ireturn,1,nhelix,num1,num2,
     *                 subunit,nnhelix,nnum1,nnum2,i1,j1,1)
                     if(eassoc.lt.-0.50) write(*,'(2(''subunit '', 
     *                 a1,'' helix'',i4,'','',2x),f6.1,'' kcal/mol'')')
     *                 subunit(i1),i,subunit(j1),j,eassoc
                     eij(i1,j1)=eij(i1,j1)+eassoc
                  end do
               end do
               ediss=ediss+eij(i1,j1)
c
c              association energy of two subunits i1 and j1:
c
               call bind(pdbtempl,subunit(i1),subunit(j1),
     *           numtm1,numtm2,eassoc,ireturn,3,nhelix,num1,num2,
     *           subunit,nnhelix,nnum1,nnum2,i1,j1,1)
               eij2(i1,j1)=eassoc
            end do
         end do
c
         write(*,'(''Energies of association of subunits: '')')
         do i1=1,nnsub-1
            do j1=i1+1,nnsub
               write (*,'(''Subunis: '',a1,''-'',a1,f6.1,
     *           '' kcal/mol'')')
     *           subunit(i1),subunit(j1),eij2(i1,j1)
            end do
         end do
c
c        association energy of all subunits:
c
         call bind(pdbtempl,subunit(i1),subunit(j1),
     *     numtm1,numtm2,ediss2,ireturn,4,nhelix,num1,num2,
     *     subunit,nnhelix,nnum1,nnum2,1,1,nnsub)
         write (*,'(''Total association energy of all subunits:'',
     *     f6.1,'' kcal/mol'')') ediss2
      end if                    
c     close(81)
      stop
c***
      nstr=mm
c
      m0=0
      do j=1,nhelix-1
         do k=j+1,nhelix
            m0=m0+1
            indpair(m0)=0
            do l=1,nstr
               if(etable(m0,l).lt.ecutoff) indpair(m0)=1
            end do
         end do
      end do
c
      m=0
      m0=0
      do j=1,nhelix-1
         do k=j+1,nhelix
            m0=m0+1
            if(indpair(m0).eq.1) then
              m=m+1
              ibuf(1,m)=j
              ibuf(2,m)=k
            end if
         end do
      end do
      nshow=m
      open (21,file='table1')
      write (21,'('';;;;'',60x,20(2i2,''; ''))') 
     *  (ibuf(1,l),ibuf(2,l),l=1,nshow)
c
      do i=1,nstr
         m=0
         m0=0
         do j=1,nhelix-1
            do k=j+1,nhelix
               m0=m0+1
               if(indpair(m0).eq.1) then
                 m=m+1
                 ebuf(m)=etable(m0,i)
               end if
            end do
         end do
         nshow=m
         etot=0.
         do l=1,nshow
            etot=etot+ebuf(l)
         end do
         write (21,'(a4,'';'',a1,'';'',a4,'';'',a,'';'',
     *     20(f5.1,'';''))')
     *     pdbtable(i),subtable(i),restable(i)(1:4),
     *     nametable(i)(1:50),(ebuf(l),l=1,nshow),etot
      end do
      stop
      end 
c
      subroutine bind (pdbtempl,subunit1,subunit2,
     *  numtm1,numtm2,eassoc,ireturn,idiss,nhelix,num1,num2,
     *  subunit,nnhelix,nnum1,nnum2,ii1,jj1,nnsub)
c     ---------------------------------------------------------
c     Dissociation free energy of N transmembrane subunits
c
c                          Conventions
c                          -----------
c        1. Use only one PDB file and the corresponding secondary
c     structure (.cor) file for the complex.  The numbering in .cor
c     file corresponds to original PDB file (all residue numbers
c     in PDB file are assumed to be unique). In the program, all
c     residues are then renumbered sequentially for the complex
c     and each subunit.
c        2. Each "structural subunit" can include pieces of several PDB
c     subunits that must go sequentially (A, B, C,etc.) in .cor file.
c     Such pieces are then combined in a single sequentially continuous 
c     segment for each subunit.
c        3. All secondary structures must be within subunit borders;
c     all transmembrane segments must be within secondary structures.
c        4. Whole subunits (not only regular secondary structures) will be
c     included for calculations of ASA and and inter-residue interactions
c     (the list for inter-residue interactions does not includes own helix).  
c        5.  "Active" residues will be selected only from regular secondary
c     stuctures (and in the present version, only from TM segments).
c     "active" residues are selected only from secondary stuctures.
c     The active residues of each subunit will be determined as
c     ones interacting to other subunits, regardless of their ASA.
c        6. All template residues must be complete.  No cofactors.
c
c                     Current approximations
c                     ----------------------
c        1. Only interactions between active residues (with softened 6-12 and
c     10-12 potentials) will be included in association energy; however,
c     all interactions (including repulsions) will be taken into account 
c     for calculation of conformer occupancies (see subroutine 'interactions'
c     in 'contrib.f').
c        2. Pairwise interactions between residues from same helix
c      include only side-chain-side-chain interactions.  They are not included
c      in 'listsur', but considered specifically as +-1,3,4 in 'contrib.f'.
c        3. No electrostatic energy, except TMH-TMH backbone interactions
c        4. The following contributions do not change during dissociation:
c           (a) repulsions (they are included only to calculate occupancies
c               of side-chain conformers).
c           (b) torsion energy.
c        5. No water-lipid interface (only two environment types: 1 - water-
c     -protein, and 2 - lipid-protein).  All residues within TM segments
c     are classified as type 2.  We use currently only type 2.
c        6. Standard side-chain conformers (how about chi from original template?)
c     All allowed side-chain conformers have identical intra-helix vdW energy.
c        7. Approximations for side-chain conformational averaging and
c     calculation of H-bonds and vdW interactions.
c        8. Conformational entropy contribution has been omitted for the
c     "rigid" (iflex=0) model!
c
c                        Parameters
c                        ----------
c     nsub     - number of subunits 
c     numsub   - numbers of first and last residues in subunits
c     nact_su  - numbers of "active" residues in subunits
c     listact  - numbers of "active" residues in each subunit
c     maxsub   - maximal number of subunuts
c     maxact   - maximal number of active residues
c
      include 'template.inc'
c
      parameter (maxact=600,maxsub=25)
c
      character*80 reslib1,rotlib,envfile
      character*180 pdbtempl,pdbout
c
      integer numsub(2,maxsub),nact_su(maxsub),listact(maxsub,maxact),
     *  isub(2,maxsub),iact(maxact),isegm0(2,maxsegm),numtm0(2,maxsegm),
     *  numtm1(1),numtm2(1),num1(1),num2(1),nnum1(100,1),nnum2(100,1),
     *  nnhelix(1)
      character*1 subunit1,subunit2,subunit(1)
c
      common /inpar/ wrepuls
c
      iprint=1
      iflex=1
c
c     iflex =0 - no averaging of side-chain conformers
c            1 - averaging of individual side-chains
c
      wrepuls=3.0
c
c     Input files:
c
      reslib1='res1.lib'
c     write(*,'(''Residue library  '',a50)') reslib
      rotlib='rothel1.lib'
c     write (*,'(''Rotamer library:  '',a50)') rotlib
c
c     1. Read parameters of residues ('residues.inc')
c     (arbitrary order of residues in 'reslib' and 'rotlib')
c
      call read_res (reslib1,iprint)
c
c     2. Read rotamer library
c
      call read_rota (rotlib,iprint)
c
c     write (*,'(a50)') pdbtempl
      do j=2,77
         if(pdbtempl(j:j+3).eq.'.pdb') then
            envfile=pdbtempl(1:j-1)//'.csd'
            pdbout=pdbtempl(1:j-1)//'out.pdb'
            go to 10
         end if
      end do
      write (*,'(''wrong name of PDB file:'',a50)') pdbtempl
      stop
   10 continue
c
c     if(iprint.ge.1) open (15,file='work.pdb')
c     (coordinates of NH hydrogens - for testing)
c
c     3. Read and renumber in sequential order all residues from
c     PDB file, and the corresponding borders of subunits, secondary
c     structures, and transmembrane segments. This is for the entire 
c     complex. The intermediate PDB file will be written into 'pdbout'.
c
      call redefine (iprint,nsub,numsub,pdbtempl,pdbout,
     *  subunit1,subunit2,numtm1,numtm2,ireturn,idiss,nhelix,
     *  num1,num2,subunit,nnhelix,nnum1,nnum2,ii1,jj1,nnsub)
c
      if(ireturn.eq.1) then
c        if(iprint.ge.1)close(15)
         return
      end if
c
c     4. Determine active residues in each "structural subunit"
c 
      call active (iprint,pdbout,nsub,numsub,nact_su,listact)
c
c     The continuous list of residues has been written to 'pdbout'
c     for the entire complex. SS and TM segments are not assigned
c     to specific "structural subunits" at this moment.
c
      nsegm0=nsegm
      do i=1,nsegm
         isegm0(1,i)=isegm(1,i)
         isegm0(2,i)=isegm(2,i)
      end do
      ntm0=ntm
      do i=1,ntm
         numtm0(1,i)=numtm(1,i)
         numtm0(2,i)=numtm(2,i)
      end do
c
c     Kept for the entire complex:
c     1. nsub,numsub(2,nsub)
c     2. nsegm0,isegm0(2,nsegm0)
c     3. ntm0,numtm0(2,ntm0)
c     4. nact_su(nsub), listact(nsub,nact_su(nsub))
c     
c     Used for the complex and subunits while calling 'deltaG.f':
c     1. nsub (1 for subunits),isub(2,nsub) (nbord,ibord within 'deltaG')
c     2. nsegm, isegm(2,nsegm)
c     3. ntm, numtm(2,ntm)
c     4. nact, iact(nact)
c
c     5. Calculate delta G for the entire complex (all indicated 
c     subunits and active residues included) 
c
      m=0
      do i=1,nsub
         isub(1,i)=numsub(1,i)
         isub(2,i)=numsub(2,i)
         do j=1,nact_su(i)
            m=m+1
            if(m.gt.maxact) then
               write (*,'(''Too many active residues'')')
               stop
            end if
            iact(m)=listact(i,j)
         end do
      end do
      nact=m
      nact_tot=nact
      call deltaG (0,iprint,pdbout,envfile,nsub,isub,
     *  nact,iact,eres_compl,entropy_compl,ehd_compl,ehbonds_compl,
     *  iflex,etransf_compl,ireturn)
      if(ireturn.eq.1) return
c
c     6. Calculate deltaGs for the individual "subunits"
c
      eres_tot=0.
      entropy_tot=0.
      ehd_tot=0.
      ehbonds_tot=0.
      etransf_tot=0.
      nact=0
      do i=1,nsub
         isub(1,1)=numsub(1,i)
         isub(2,1)=numsub(2,i)
         do j=1,nact_su(i)
            iact(j)=listact(i,j)
         end do
         nact=nact_su(i)
c
c        Select secondary structures and transmembrane segments
c        that are included in complex or subunit 'i'
c
         m=0
         do j=1,nsegm0
            if(isegm0(1,j).ge.isub(1,1).and.
     *         isegm0(2,j).le.isub(2,1)) then
               m=m+1
               isegm(1,m)=isegm0(1,j)
               isegm(2,m)=isegm0(2,j)
            end if
         end do
         nsegm=m
         if(nsegm.eq.0) then
            write (*,'(''No secondary structures'')')
            stop
         end if
c
         m=0
         do j=1,ntm0
            if(numtm0(1,j).ge.isub(1,1).and.
     *         numtm0(2,j).le.isub(2,1)) then
               m=m+1
               numtm(1,m)=numtm0(1,j)
               numtm(2,m)=numtm0(2,j)
            end if
         end do
         ntm=m
         if(ntm.eq.0) then
            write (*,'(''No transmembrane segments'')')
            stop
         end if
c
         call deltaG (i,iprint,pdbout,envfile,1,isub,
     *     nact,iact,eres_sub,entropy_sub,ehd_sub,ehbonds_sub,
     *     iflex,etransf_sub,ireturn)
         if(ireturn.eq.1) return
         eres_tot = eres_tot + eres_sub
         entropy_tot = entropy_tot + entropy_sub
         ehd_tot = ehd_tot + ehd_sub
         ehbonds_tot=ehbonds_tot+ehbonds_sub
         etransf_tot=etransf_tot+etransf_sub
      end do
      eres_fin=eres_compl - eres_tot
      entropy_fin=entropy_compl - entropy_tot
      ehd_fin=ehd_compl - ehd_tot
      ehbonds_fin=ehbonds_compl - ehbonds_tot
      etransf_fin=etransf_compl - etransf_tot
      efin=eres_fin + entropy_fin + ehd_fin + etransf_fin
      eaver=efin/float(nact_tot)
      e_vdw=eres_fin - ehbonds_fin
c*    write (*,'(''vdW:'',f8.1,''  H-b:'',f8.1,
c*   *  '' Entropy:'',f6.1,''  Transf:'',f6.1,'' Dipole:'',f6.1,)')
c*   *  e_vdW, ehbonds_fin,entropy_fin,etransf_fin,ehd_fin
c*    write (*,'(''Total energy:'',f8.2,f8.3)') efin, eaver
      eassoc=efin
      return
      end
c
      subroutine corbound (pdbtempl,subunit,nhelix,num1,num2,ireturn,
     *  nat,numat,namat,namres,xyz,iresol,uniprot)
c     --------------------------------------------------------------
c     Read amino acid sequence of the specified subunit from the pdb 
c     file and redefine boundaries of TM segments if needed. The 
c     numbers of residues (num1, num2) are defined as in the PDB file. 
c
c     This subroutine is needed only to: (a) read coordinates for
c     writing them for nucleations; (b) check aa sequence and
c     secondary structure by making a comparison with DSSP SS+disorder
c     (missing segments) assignments. 'seq1' and 'numres' are only
c     used here; they do not go anywhere.
c
      parameter (maxres=8000,maxhel=50,maxsol=50,maxbet=50,maxdis=50)
c
      character*80 line
      character*180 pdbtempl
      character*1 subunit,namsu,seqdssp(maxres),ss(maxres),
     *  sstmh0(maxres),sstmh(maxres),disorder(maxres)
      integer num1(1),num2(1),numres(maxres),numres1(maxres),numat(1),
     *  num1loc(maxhel),num2loc(maxhel),
     *  numt1(maxhel),numt2(maxhel),numts1(maxhel),numts2(maxhel),
     *  numsol1(maxsol),numsol2(maxsol),numbet1(maxbet),numbet2(maxbet),
     *  numdis1(maxdis),numdis2(maxdis)
      character*1 res1(20)/'L','I','V','F','M','P','A','G','C','W',
     *  'T','S','Y','N','Q','H','K','R','E','D'/,seq1(maxres),
     *  seqpol(7)/'N','Q','H','K','R','E','D'/,seqloc(100),
     *  seqpol2(9)/'S','G','N','Q','H','K','R','E','D'/
      character*4 res4(20)/'LEU ','ILE ','VAL ','PHE ','MET ','PRO ',
     *  'ALA','GLY ','CYS ','TRP ','THR ','SER ','TYR ','ASN ',
     *  'GLN ','HIS ','LYS ','ARG ','GLU ','ASP '/,seq4(maxres),
     *  namres(1),namr,namat(1),nama
      character*6 uniprot
      real xyz(3,1)
      data npol/7/,ndat/20/
c
c     read amino acid sequence and residue numbers of subunit
c     from the pdb file
c
      open(51,file=pdbtempl)
      m=0
      mat=0
      do i=1,180000
         read (51,'(a)',end=20) line
         if(line(1:6).eq.'ENDMDL'.or.line(18:20).eq.'DUM') go to 20
         if(line(1:6).eq.'HETATM') then
            read (line,'(22x,i4)') ntest
            if(ntest.gt.1) go to 10
         else 
            if(line(1:4).ne.'ATOM') go to 10
            if(line(14:14).eq.'H'.or.line(14:14).eq.'D'.or.
     *        line(13:13).eq.'H'.or.line(14:14).eq.'Q') go to 10
            if(line(17:17).ne.' '.and.line(17:17).ne.'A') go to 10
         end if
         read (line,'(13x,2a4,a1,i4,4x,3f8.3)') nama,namr,namsu,numr,
     *     x,y,z
         if(namsu.eq.subunit) then
            mat=mat+1
            numat(mat)=numr
            namat(mat)=nama
            namres(mat)=namr
            xyz(1,mat)=x
            xyz(2,mat)=y
            xyz(3,mat)=z
            if(mat.gt.50000) then
               write (*,'(''Too many atoms in subunit'')')
               stop
            end if
            if(m.ge.1) then
               if(numr.ne.numr0) then
                  m=m+1
                  if(m.gt.maxres) then
                     write (*,'(''Too many residues'')')
                     stop
                  end if
                  seq4(m)=namr
                  numres(m)=numr
                  numr0=numr
               end if
            else
               m=m+1
               numr0=numr
               seq4(1)=namr
               numres(1)=numr
            end if
         end if
   10    continue
      end do
   20 continue
      nres=m
      nat=mat
      close(51)
      return
c***
c     write (*,'(i6)')nres
c
c     Make single-letter code sequence
c
      do i=1,nres
         do j=1,ndat
            if(seq4(i).eq.res4(j)) then
               seq1(i)=res1(j)
               go to 30
            end if
         end do
         write (*,'(''residue not found: '',a4)') seq4(i)
   30    continue
      end do
c
c     read aa sequence and secondary structure
c
      read(*,'(8000a1)') (seqdssp(i),i=1,maxres)
      do i=1,maxres
         if(seqdssp(i).eq.' ') then
            nres2=i-1
            go to 35
         end if
      end do
   35 continue
      read(*,'(8000a1)') (ss(i),i=1,nres2)
      read(*,'(8000a1)') (disorder(i),i=1,nres2)
      ss(1)=' '
      do i=1,nres2
         if(ss(i).eq.'T') ss(i)='H'
      end do
      if(seqdssp(1).eq.'X') then
         do i=2,nres2
            seqdssp(i-1)=seqdssp(i)
            ss(i-1)=ss(i)
            disorder(i-1)=disorder(i)
         end do
         nres2=nres2-1
      end if
      write (91,'(2000a1)') (seqdssp(l),l=1,nres2)
c
c     Redefine numbers of 1st and last residues in TMH segments 
c     according to the continuous sequence numbering starting from 
c     1st residue. This is aa sequnce taken from PDB file.
c
      do i=1,nhelix
         m=0
         do j=1,nres
            if(numres(j).eq.num1(i)) then
               num1loc(i)=j
               m=m+1
            end if
            if(numres(j).eq.num2(i)) then
               num2loc(i)=j
               m=m+1
            end if
         end do
         if(m.ne.2) then
            write (*,'(''helix boundaries are undefined: '',
     *        a4,1x,a1,3i5)') 
     *        pdbtempl(1:4),subunit,i,num1(i),num2(i)
            go to 40
         end if
      end do
      write (*,'(''helices'',60i4)') (num1loc(i),num2loc(i),i=1,nhelix)
c
      do i=1,nres
         sstmh0(i)='-'
      end do
      do i=1,nhelix
         do j=num1loc(i),num2loc(i)
            sstmh0(j)='+'
         end do
      end do
c
      m=0
      do i=1,nres2
         if(disorder(i).ne.'X') then
            m=m+1
c           if(seqdssp(i).ne.seq1(m)) then
c              write(*,'(''inconsistent sequence: '',a4,3(1x,a1),2i5)')
c    *           pdbtempl(1:4),subunit,seqdssp(i),seq1(m),i,numres(m)
c              go to 40
c           end if
            sstmh(i)=sstmh0(m)
            numres1(i)=numres(m)
         else
            sstmh(i)='-'
            numres1(i)=0
         end if
      end do
      if(m.ne.nres)  write (*,'(''inconsistent number of residues: '',
     *  2i5)') nres,m
c
      indic=0
c     if(iresol.le.3) then
      do i=2,nres2-1
         if(sstmh(i).eq.'+'.and.ss(i).eq.' ') then
            if((sstmh(i-1).eq.'+'.and.sstmh(i+1).eq.'+').or.
     *         (sstmh(i-1).eq.'+'.and.ss(i-1).eq.' ').or.
     *         (sstmh(i+1).eq.'+'.and.ss(i+1).eq.' ')) then
               write (*,'(''inconsisitent sec. structure: '',
     *           a4,2(1x,a1),2i5)') 
     *           pdbtempl(1:4),subunit,seqdssp(i),numres1(i)
               indic=indic+1
            else
               do l=1,9
                  if(seqpol2(l).eq.seq1(i)) 
     *              write (81,'(a4,2(1x,a1),2i5)') 
     *           pdbtempl(1:4),subunit,seqdssp(i),numres1(i)
               end do
            end if
         end if
      end do
      if(indic.ge.1) then
         do i=1,nres2
            write (*,'(i5,3(1x,a1),i5)') 
     *        i,seqdssp(i),sstmh(i),ss(i),numres1(i)
         end do
      end if
c     end if
   40 continue
c
c     Define new numbers of 1st and last residues for TM segments
c
      m=0
      do i=1,nres2-5
         if(sstmh(i).eq.'-'.and.sstmh(i+1).eq.'+') then
            m=m+1
            numt1(m)=i+1
         end if
         if(sstmh(i).eq.'+'.and.sstmh(i+1).eq.'-') numt2(m)=i
      end do
      if(m.ne.nhelix) write (*,'(''inconsistent number of TMH '',a4,
     *  1x,a1)') pdbtempl(1:4),subunit
c
c     Define numbers of 1st and last residues for TM 
c     and other helices
c
      do i=1,nhelix
         numts1(i)=numt1(i)
         numts2(i)=numt2(i)
      end do
      m=0
      do i=1,nres2-5
         if(ss(i).ne.'H'.and.ss(i+1).eq.'H') then
c           could be also 'T' here!
            m=m+1
            numsol1(m)=i+1
         end if
         if(ss(i).eq.'H'.and.ss(i+1).ne.'H') then
            numsol2(m)=i
            if(numsol2(m)-numsol1(m).le.3) then
               m=m-1
               go to 50
            end if
c 
c           check overlap with TM helices:
c
            do j=1,nhelix
               if(numsol1(m).le.numt1(j).and.
     *           numsol2(m).ge.numt2(j)) then
                  numts1(j)=numsol1(m)
                  numts2(j)=numsol2(m)
                  m=m-1
                  go to 50
               end if
               if(numsol1(m).ge.numt1(j).and.
     *           numsol2(m).le.numt2(j)) then
                  m=m-1
                  go to 50
               end if
               if(numsol1(m).lt.numt1(j).and.
     *           numsol2(m).ge.numt1(j)) then
                  numts1(j)=numsol1(m)
                  m=m-1
                  go to 50
               end if
               if(numsol1(m).le.numt2(j).and.
     *           numsol2(m).gt.numt2(j)) then
                  numts2(j)=numsol2(m)
                  m=m-1
                  go to 50
               end if
            end do
   50       continue
         end if
      end do
      nhelsol=m
c
c     Define numbers of 1st and last residues for beta-strands
c     Can be problems with beta-bulges!
c
      m=0
      do i=1,nres2-1
         if(ss(i).ne.'E'.and.ss(i+1).eq.'E') then
            m=m+1
            numbet1(m)=i+1
         end if
         if(ss(i).eq.'E'.and.ss(i+1).ne.'E') then
            numbet2(m)=i
            if(numbet2(m)-numbet1(m).le.1) m=m-1
         end if
      end do
      if(ss(nres2).eq.'E') numbet2(m)=nres2
      nbet=m
c
c     Define numbers of 1st and last residues in segments not 
c     included in the PDB file 
c
      m=0
      if(disorder(1).eq.'X') then
         m=1
         numdis1(1)=1
      end if
      do i=1,nres2-1
         if(disorder(i).ne.'X'.and.disorder(i+1).eq.'X') then
            m=m+1
            numdis1(m)=i+1
         end if
         if(disorder(i).eq.'X'.and.disorder(i+1).ne.'X') numdis2(m)=i
      end do
      if(disorder(nres2).eq.'X') numdis2(m)=nres2
      ndis=m
c
      write (*,'(a6)') uniprot
      write (91,'(i3,''|'',20(''('',i4,''-'',i4,''),''))') 
     *  nhelix,(numt1(i),numt2(i),i=1,nhelix)
      write (91,'(i3,''|'',20(''('',i4,''-'',i4,''),''))') 
     *  nhelix,(numts1(i),numts2(i),i=1,nhelix)
      if(nhelsol.ge.1) then
         write (91,'(i3,''|'',20(''('',i4,''-'',i4,''),''))') 
     *  nhelsol,(numsol1(i),numsol2(i),
     *     i=1,nhelsol)
      else
         write (91,'(i3,''|'')') nhelsol
      end if
      if(nbet.ge.1) then
         write (91,'(i3,''|'',20(''('',i4,''-'',i4,''),''))') 
     *  nbet,(numbet1(i),numbet2(i),i=1,nbet)
      else
         write (91,'(i3,''|'')') nbet
      end if
      if(ndis.ge.1) then
         write (91,'(i3,''|'',20(''('',i4,''-'',i4,''),''))') 
     *  ndis,(numdis1(i),numdis2(i),i=1,ndis)
      else
         write (91,'(i3,''|'')') ndis
      end if
c 
      return
c     ****
c     do i=1,nres
c        write (*,'(a1,1x,a4,1x,a1,i6)') subunit,
c    *     seq4(i),seq1(i),numres(i)
c     end do
c
c     Redefine boundaries of TM segments if needed
c
      do i=1,nhelix
c
c        make local sequence of TM segment:
c
         m=0
         do j=1,nres
            if(numres(j).ge.num1(i).and.numres(j).le.num2(i)) then
               m=m+1
               seqloc(m)=seq1(j)
            end if
         end do
         if(m.ne.num2(i)-num1(i)+1) then
            write (*,'(''inconsistent segment: '',a8,1x,a1,2i5)') 
     *        pdbtempl(1:8),subunit,num1(i),num2(i)
            ireturn=1
            return
c           stop
         end if
         nloc=m
c
c        fix ends:
c
         num0=num1(i)
         n1=0
         do k=1,4
            do l=1,npol
               if(seqloc(k).eq.seqpol(l)) then
                  num1(i)=num0+k
                  n1=k
               end if
            end do
         end do
         num0=num2(i)
         n2=0
         do k=1,4
            do l=1,npol
               if(seqloc(nloc-k+1).eq.seqpol(l)) then
                  num2(i)=num0-k      
                  n2=k
               end if
            end do
         end do
         write (*,'(2i5,2i2,1x,60a1)') num1(i),num2(i),
     *     n1,n2,(seqloc(k),k=1,nloc)
      end do
      return
      end
c
      subroutine pathway (nhelix,num1,num2,ematr,
     *  nat,numat,namat,namres,xyz,subunit,pdbpath)
c     ----------------------------------------------------
c     Determine intermediate products and the co-translational 
c     assenbly pathway of TM helices using 3D structure of the protein 
c
c                       Rules:
c     1. After addition of n-th TMH all nucleations (a single TMH 
c     is also a nuclation) with DGassoc < ecutfold will associate.
c     2. The newly added TMH or a nucleation associates only 
c     with the sequentially adjacent nucleation
c
c                 Parameters of nucleations:
c     nucl - number of nucleations
c     nhel(maxnuc) - number of TM helices included in the nucleation
c     numhel(maxhel,maxnuc) - numbers of TM helices in the nucleation
c     enuc(maxnuc) - total energy of interhelix interacton in 
c                   of the nucleation
c     equat(maxnuc) - text equation that describes assembly pathawy
c                   of the nucleation
c
      parameter(maxhel=50,maxnuc=30)
      real epair(1),xyz(3,1),ematr(maxhel,1),enuc(maxnuc),
     *  ecut(4)/-7.0,-4.,-2.,-1./
c    *  ecut(7)/-15.,-10.,-8.,-6.,-4.,-2.,-1./
      integer ipair(2,1),num1(1),num2(1),numat(1),
     * nhel(maxnuc),numhel(maxhel,maxnuc)
      character*256 equat(maxnuc)
      character*800 path
      character*180 pdbpath
      character*4 namat(1),namres(1)
      character*1 subunit
c
      write (*,'(''Helix-helix association free energies:'')')
      write (*,'(3x,20i6)') (j,j=1,nhelix)
      do i=1,nhelix
         write (*,'(i4,20f6.1)') i,(ematr(j,i),j=1,nhelix)
      end do
      write (*,'(''TM helix assembly pathway:'')')
c
c     1. Add TM helixes one by one, and every time determine
c     nucleations:
c
      iterglo=1
      ecutfold=ecut(1)
c
      open (61,file=pdbpath)
      istep=0
      nucl=1
      nhel(1)=1
      numhel(1,1)=1
      enuc(1)=0.
      equat(1)='1'
      do i=2,nhelix
         call addhel (i,nucl,nhel,numhel,enuc,equat,num1,num2,
     *     ematr,nat,numat,namat,namres,xyz,istep,subunit,
     *     ecutfold,iterglo)
c        do j=1,nucl
c           write (*,'(''#'',i3,'' Nucleation'',i3,f6.1,1x,a)') 
c    *        i,j,enuc(j),equat(j)(1:70)
c           nn=nhel(j)
c           write (*,'(4x,i3,20i4)') nn,(numhel(k,j),k=1,nn)
c        end do
      end do
      do i=1,4
         iterglo=iterglo+1
         if(nucl.ge.2.and.ecutfold.le.-2.0) then
            ecutfold=ecut(iterglo)
c           write (*,'(''reduced cutoff:'',f6.1)') ecutfold
            call addhel (i,nucl,nhel,numhel,enuc,equat,num1,num2,
     *        ematr,nat,numat,namat,namres,xyz,istep,subunit,
     *        ecutfold,iterglo)
         end if
      end do
      close (61)
c
c     2. Output final pathway. Some nucleations may not be merged.
c
      path=equat(1)
      if(nucl.ge.2) then 
         do i=2,nucl
            ii1=index(path,'   ')-1
            ii2=index(equat(i),'   ')-1
            path=path(1:ii1)//','//equat(i)(1:ii2)
         end do
      end if
      ii1=index(path,'   ')-1
c     write (91,'(a)') path(1:ii1)
c     write (91,'('' '')') 
c     i00=index(pdbpath,'.pdb')-1
      write (*,'(''Pathway: '',a)') path(1:ii1)
      return
      end
c
      subroutine addhel (ihel,nucl,nhel,numhel,enuc,equat,
     *  num1,num2,ematr,nat,numat,namat,namres,xyz,istep,
     *  subunit,ecutfold,iterglo)
c     ---------------------------------------------------
c     Calculate nucleations after addition of TM helix ihel
c
      parameter(maxhel=50,maxnuc=30)
      real e(maxnuc,maxnuc) 
      character*2 namhel(maxhel)/'1 ','2 ','3 ','4 ','5 ','6 ','7 ',
     *  '8 ','9 ','10','11','12','13','14','15','16','17','18','19',
     *  '20','21','22','23','24','25','26','27','28','29','30',
     *  '31','32','33','34','35','36','37','38','39','40',
     *  '41','42','43','44','45','46','47','48','49','50'/
      real xyz(3,1),ematr(maxhel,1),enuc(1)
      integer num1(1),num2(1),numat(1),nhel(1),numhel(maxhel,1)
      character*256 equat(1)
      character*4 namat(1),namres(1)
      character*1 subunit
c
c     Define newly added TMH as an additional nucleation
c
      if(iterglo.eq.1) then
         nucl=nucl+1
         nhel(nucl)=1
         numhel(1,nucl)=ihel
         enuc(nucl)=0.
         equat(nucl)=namhel(ihel)
      end if
c
c     Repeat several mergings until convergence
c
      imerge=1
      do iter=1,10
         if(imerge.eq.0) go to 20
         imerge=0
c
c        define interaction energies between nucleations
c
         do i=1,nucl
            e(i,i)=0.
         end do
         do i=1,nucl-1
            do j=i+1,nucl
               e(i,j)=0.
               do k=1,nhel(i)
                  do l=1,nhel(j)
                     e(i,j)=e(i,j)+ematr(numhel(k,i),numhel(l,j))
                     e(j,i)=e(i,j)
                  end do
               end do
            end do
         end do
c
c        Find sequentially adjacent nucleations with the lowest
c        interaction energy to be merged
c
         emin=0.
         do i=1,nucl-1
            do j=i+1,nucl
c              are they sequentially adjacent?
               ind=0
               do k=1,nhel(i)
                  do l=1,nhel(j)
                     if(iabs(numhel(k,i)-numhel(l,j)).eq.1) ind=1
                  end do
               end do
               if(ind.eq.1.and.e(i,j).lt.ecutfold) then
                  if(e(i,j).lt.emin) then
                     emin=e(i,j)
                     imin=i
                     jmin=j
                   end if
               end if
            end do
         end do
         if(emin.lt.ecutfold) then
            call merge_nuc (imin,jmin,emin,nucl,nhel,numhel,enuc,equat,
     *        num1,num2,nat,numat,namat,namres,xyz,istep,subunit)
            imerge=1
         end if
c
c        check if other nucleations can be merged -
c        find nucleations with lowest interaction energy
c
c        emin=0.
c        do i=1,nucl-1
c           do j=i+1,nucl
c              if(e(i,j).lt.emin) then
c                 emin=e(i,j)
c                 imin=i
c                 jmin=j
c               end if
c           end do
c        end do
c        if(emin.lt.ecutfold) then
c           call merge_nuc (imin,jmin,emin,nucl,nhel,numhel,enuc,equat,
c    *        num1,num2,nat,numat,namat,namres,xyz,istep,subunit)
c           imerge=1
c        end if
      end do
   20 continue
      return
      end 
c
      subroutine merge_nuc (inuc,jnuc,eint,nucl,nhel,numhel,enuc,equat,
     *  num1,num2,nat,numat,namat,namres,xyz,istep,subunit)
c     ----------------------------------------------------------------
c     Merging two nucletaions during assembly. Each merging is 
c     considered a step of the assembly pathway.
c
      parameter(maxhel=50)
      real xyz(3,1),enuc(1)
      integer num1(1),num2(1),numat(1),nhel(1),numhel(maxhel,1)
      character*256 equat(1)
      character*4 namat(1),namres(1)
      character*1 subunit
c
      istep=istep+1
c
c     1. Merge lists of helices
c
      do i=1,nhel(jnuc)
         numhel(nhel(inuc)+i,inuc)=numhel(i,jnuc)
      end do
      nhel(inuc)=nhel(inuc)+nhel(jnuc)
c
c     2. Modify folding equation for nucleation 'inuc'
c
      ind1=index(equat(inuc),'    ')-1
      ind2=index(equat(jnuc),'    ')-1
      equat(inuc)='('//equat(inuc)(1:ind1)//'+'//
     *  equat(jnuc)(1:ind2)//')'
c
c     3. Redefine energy of nucleation 'inuc'
c
      enuc(inuc)=enuc(inuc)+enuc(jnuc)+eint
c
c
c     4. Redefine numbering of nucleations after removing
c     nucleation 'jnuc'
c
      if(jnuc.lt.nucl) then
         do i=jnuc+1,nucl
            nhel(i-1)=nhel(i)
            do j=1,nhel(i-1)
               numhel(j,i-1)=numhel(j,i)
            end do
            enuc(i-1)=enuc(i)
            equat(i-1)=equat(i)
         end do
      end if
      nucl=nucl-1
c
c     5. Write  3D coordinates of the nucleation. Here we include loops
c     assuming that the joined TM segments are sequential
c
      write (61,'(''MODEL'',i9)') istep
      write (61,'(''REMARK '',''DGasc='',f7.1,
     *  '' kcal/mol'')') enuc(inuc)
      ii1=index(equat(inuc),'    ')-1
      write (61,'(''REMARK '',''Pathway:'',a)') equat(inuc)(1:ii1)
      write (*,'(''Step'',i3,f6.1,2x,a)') istep,enuc(inuc),
     *  equat(inuc)(2:ii1-1)
c     
      natmin=nat
      natmax=1
      do j=1,nhel(inuc)
         n=numhel(j,inuc)
         if(num1(n).lt.natmin) natmin=num1(n)
         if(num2(n).gt.natmax) natmax=num2(n)
      end do
      m=0
      do i=1,nat
         if(numat(i).ge.natmin.and.numat(i).le.natmax) then
            m=m+1
            write(61,'(''ATOM  '',i5,2x,2a4,
     *        a1,i4,4x,3f8.3)') m,namat(i),namres(i),
     *        subunit,numat(i),(xyz(l,i),l=1,3)
         end if
      end do
      write (61,'(''ENDMDL'')')
      return
      end
c
      subroutine subst(resw,nmut,resm,pdbtempl,pdbtempl2)
c     ---------------------------------------------------
      include 'residues.inc'
c
      character*80 pdbtempl,pdbtempl2,line
      character*4 resw,resm,namat_cur,namres_cur
      integer ind(maxat_res)
c
      i1=index(pdbtempl,'.pdb')
      pdbtempl2=pdbtempl(1:i1-1)//'mut.pdb'
      open (11,file=pdbtempl)
      open (12,file=pdbtempl2)
c      
      do i=1,maxat_res
         ind(i)=0
      end do
c
      do ii=1,20000
         read (11,'(a)',end=20) line
         if(line(1:4).eq.'ATOM') then
            if(line(14:14).eq.'H'.or.line(14:14).eq.'D'.or.
     *        line(13:13).eq.'H'.or.line(14:14).eq.'Q') go to 10
            if(line(17:17).eq.' '.or.line(17:17).eq.'A') then
               read (line,'(13x,a4,a4,1x,i4,4x)')
     *           namat_cur,namres_cur,numres_cur
               if(numres_cur.eq.nmut) then
                  do i=1,max_res
                     if(namres_full(i).eq.resm) then
                        natr=natres(i)
                        ir=i
                        do j=1,natr
                           if(namat_cur.eq.namat(j,i)) then
                              write (12,'(a17,a4,a59)') 
     *                          line(1:17),resm,line(22:80)
                              ind(j)=1
                           end if
                        end do
                     end if
                  end do
               else
                  write (12,'(a)') line
               end if
            end if
   10       continue
         else
            write (12,'(a)') line
         end if
      end do
   20 continue
      do i=1,natr
         if(ind(i).eq.0) then
            write (*,'(''atom not used for replacement:'',
     *        a4,1x,a4)') namres_full(ir),namat(i,ir)
         end if
      end do
      close (11)
      close (12)
      return
      end
