      subroutine contrib (jres,ipos,etransfer,eresres,ehbown,
     *  entropy,ehbtot,iprint,nact,iact,iflex,ireturn)
c     ------------------------------------------------------
c     Calculate all energy contributions of residue 'jres' in 
c     template position 'ipos' to stability of complex or a subunit 
c
c                     Approximations 
c                     --------------
c        1. No electrostaic interactions and charge transfer energy.
c        2. Interactions across lipid, water, and protein are the same. 
c        3. Old version for H-bonds: unlimited number of partners that
c           satisfy the distance and angle cutoffs. New version: keep
c           only one, the lowest energy H-bond for each side-chain atom -
c           for calculating stability (use old version for calculating
c           conformer occupancies).
c        4. Probe radius of 1.4 A for ASA in lipid.
c        5. No angle criteria for SS bonds.
c            
c                  Output parameters:
c                  -----------------
c     etransfer - transfer energy of active residue jres
c     eresres   - its stabilizing interaction energy (vdW, H-bonds, SS;
c                 but no repulsions) with other active residues
c     ehbown    - H-bond energy of side-chain jres with backbone of own
c                 alpha-helix
c     entropy   - conformational entropy of side-chain jres
c
c       Contributions for individual conformers of side-chain jres: 
c       ----------------------------------------------------------
c     epair     - same as 'eresres' 
c     einter    - energy of interactions with all (not only active)
c                 surrounding residues, including repulsions. It is
c                 used only for calculating conformer occupancies
c                 and side-chain conformational entropy.
c     ehb_bb    - same as 'ehbown'
c     etransf   - same as 'etransfer'
c
c                   Others:
c     ienv     - environmental classes of residues in template
c     iseq     - corresponding numbers of template residues
c                in residue library
c     vect     - translation vector for each position in template
c                to calculate replaced side-chain coordinates
c     rot      - rotational transformation matrix for each position
c     xyztmpl  - template coordinates, all nonhydrogen atoms
c     ienvat   - "environments" of template atoms 
c     listres  - list of residues (i and all surrounding residues)
c                for calculations of ASA
c     nlistres - number of residues in this list
c     listsur  - list of residues surrounding i, excluding own helix
c                for calculations of Eij (vdW, H-bonds, and repulsions)
c     nlistsur - number of residues in this list
c
      include 'template.inc'
      include 'residues.inc'
c
      real xyzc(3,maxat_res), econf(maxcnf_res),occup(maxcnf_res),
     *  epair(maxcnf_res),etransf(maxcnf_res),einter(maxcnf_res),
     *  ehb_bb(maxcnf_res),ehb_tot(maxcnf_res),xyzc0(3,maxat_res)
c
      integer iact(1)
c
      data blz/0.001987/,temper/300./,pH/7.0/
c
      if(iprint.gt.2) write (*,'(''deltaG:'',2i4)') ipos,jres
      edelta=0.
c
c     1. Use backbone coordinates of replaced residue from template:
c
      n1=natres(jres)
      n2=natres(iseq(ipos))
      do l=1,3
         xyzc(l,1)=xyztmpl(l,1,ipos)
         xyzc(l,2)=xyztmpl(l,2,ipos)
         xyzc(l,n1-1)=xyztmpl(l,n2-1,ipos)
         xyzc(l,n1)=xyztmpl(l,n2,ipos)
         xyzc0(l,1)=xyzc(l,1)
         xyzc0(l,2)=xyzc(l,2)
         xyzc0(l,n1-1)=xyzc(l,n1-1)
         xyzc0(l,n1)=xyzc(l,n1)
      end do
c
      if(seq(ipos).ne.namres_full(jres)) then
         write (*,'(''Error: residue'',i4,'' in position'',i4)') 
     *    jres,ipos
         stop
      end if
c
c     Gly:
c
      if(natsch(jres).eq.0) then
         call solvation(jres,ipos,xyzc,epair(1),etransf(1),
     *     einter(1),ehb_bb(1),ehb_tot(1),iprint,nact,iact)
         etransfer=etransf(1)
         eresres=0.5*epair(1)
         ehbown=0.
         entropy=0.
         ehbtot=0.
         return
      end if
c
c     Coordinates of side-chain conformer from template:
c
      do k=3,natres(jres)-2
         do l=1,3
            xyzc0(l,k)=xyztmpl(l,k,ipos)
         end do
      end do
c*    if(iprint.gt.2) then
         do i=1,natres(jres)
            write (15,'(''ATOM'',i7,2x,2a4,1x,i4,4x,3f8.3)')
     *        i,namat(i,jres),namres_full(jres),ipos,
     *        (xyzc0(l,i),l=1,3)
         end do
c*    end if
c
c     Find the corresponding standard conformer:
c
      rmsmin=999.
c**   imin=0
      imin=1
      do i=1,nconf(jres)
         rms=0.
         do k=1,natsch(jres)
            do l=1,3
               xyzc(l,k+2)=vect(l,ipos)+
     *           rot(l,1,ipos)*(xyz_conf(1,k,i,jres)-v1(1)) +
     *           rot(l,2,ipos)*(xyz_conf(2,k,i,jres)-v1(2)) +
     *           rot(l,3,ipos)*(xyz_conf(3,k,i,jres)-v1(3))
            end do
            a=xyzc(1,k+2)-xyzc0(1,k+2)
            b=xyzc(2,k+2)-xyzc0(2,k+2)
            c=xyzc(3,k+2)-xyzc0(3,k+2)
            r2=a*a+b*b+c*c
            rms=rms+r2
         end do
         if(rms.le.rmsmin) then
            rmsmin=rms
            imin=i
         end if
      end do
c     if(imin.eq.0) then
c        write (*,'(''Template conformer was not identified '')')
c        ireturn=1
c        return
c        stop
c     end if
      if(iprint.ge.2) then
         rmsmin=sqrt(rmsmin/float(nconf(jres)))
         write (*,'(''template conformer:'',i4,f6.2)') imin,rmsmin
      end if
c
c     2. Calculate vdW, H-bond, and repulsion energies
c     for each conformer of residue jres (no electrostatics)
c
      nconform=nconf(jres)
      do i=1,nconform
         if(i.eq.imin) then
            call solvation(jres,ipos,xyzc0,epair(i),etransf(i),
     *        einter(i),ehb_bb(i),ehb_tot(i),iprint,nact,iact)
c           econf(i)= einter(i) 
            econf(i)= etransf(i) + einter(i) 
         else
c
c           Transform coordinates of the standard side-chain 
c           conformer (starting from CB)
c
            do k=1,natsch(jres)
               do l=1,3
                  xyzc(l,k+2)=vect(l,ipos)+
     *              rot(l,1,ipos)*(xyz_conf(1,k,i,jres)-v1(1)) +
     *              rot(l,2,ipos)*(xyz_conf(2,k,i,jres)-v1(2)) +
     *              rot(l,3,ipos)*(xyz_conf(3,k,i,jres)-v1(3))
               end do
            end do
c
            if(iprint.gt.2) then
               do k=1,natsch(jres)
                  write (*,'(i4,3f8.3)') i,(xyzc(l,k),l=1,3)
               end do
            end if
c
            call solvation(jres,ipos,xyzc,epair(i),etransf(i),
     *        einter(i),ehb_bb(i),ehb_tot(i),iprint,nact,iact)
c           econf(i)= einter(i) 
            econf(i)= etransf(i) + einter(i) 
         end if
      end do
c
c     3. Select lowest energy conformer:
c
      emin=999.
      do i=1,nconform   
         if(econf(i).lt.emin) then
            emin=econf(i)
         end if
      end do
c
c     4. Calc. relative energies and statistical sum
c
      rt=blz*temper
      qstat=0.
      do i=1,nconform   
         econf(i)=econf(i)-emin 
         qstat=qstat+exp(-econf(i)/rt)
      end do
c
c     5. Calc. side-chain conformer occupancies:
c
      do i=1,nconform
         occup(i)=exp(-econf(i)/rt)/qstat
      end do
c
c     6. Calc. free energy contribution of the residue:
c
      etransfer=0.
      eresres=0.
      ehbown=0.
      entropy=0.
      ehbtot=0.
      do i=1,nconform
         etransfer=etransfer+occup(i)*etransf(i)
         eresres=eresres+occup(i)*0.5*epair(i)
         ehbown=ehbown+occup(i)*ehb_bb(i)
         ehbtot =ehbtot +occup(i)*0.5*ehb_tot(i)
         if(occup(i).gt.0.001) entropy=entropy +
     *      occup(i)*rt*alog(occup(i))
      end do
c
      if(iprint.gt.2) then
c        do i=1,natres(jres)
c           write (15,'(''ATOM'',i7,2x,2a4,1x,i4,4x,3f8.3)') i,
c    *       namat(i,jres),namres_full(jres),ipos,(xyzc(l,i),l=1,3)
c        end do
         write (*,'(a4,i4)') namres_full(jres),ipos
         etotal=etransfer+eresres+ehbown+entropy
         write (*,'('' transfer:'',f6.2,'' interaction:'',f6.2,
     *     ''  HB own:'',f6.2,''  entropy:'',f6.2,''total:'',f6.2)'')')
     *     etransfer,eresres,ehbown,entropy,etotal
         do i=1,nconform
            write (*,'(6f7.2)') (chi_conf(j,i,jres),j=1,nchi(jres)),
     *        econf(i),occup(i)
         end do
      end if
c
      return
      end
c
      subroutine solvation (jres,ipos,xyzc,epair,etransf,
     *  einter,ehb_bb,ehbonds,iprint,nact,iact)
c     ---------------------------------------------------
c     Calculate solvation AND interaction energy  for residue 
c     'jres' in template position 'ipos' given coordinates of
c     the residue atoms, xyzc
c
      include 'template.inc'
      include 'residues.inc'
      include 'parameters.inc'
c
      parameter (maxs=50000)
c
      integer numipos(maxat_res),iact(1)
c
c     numipos - addresses of atoms of residue jres in the list of
c     atoms applied for calculation of ASA
c
      real xyzc(3,1),xyz(maxs,3),rads(maxs),accs(maxs)
c
c     1. Prepare arrays for ASA calculations
c
      m=0
      do i=1,nlistres(ipos)
         ls=listres(i,ipos)
         is=iseq(ls)
         if(ls.eq.ipos) then
            do j=1,natres(jres)
               m=m+1
               numipos(j)=m
               rads(m)=rad_asa(kndat(j,jres))
               xyz(m,1)=xyzc(1,j)
               xyz(m,2)=xyzc(2,j)
               xyz(m,3)=xyzc(3,j)
            end do
         else
            do j=1,natres(is)
               m=m+1
               rads(m)=rad_asa(kndat(j,is))
               xyz(m,1)=xyztmpl(1,j,ls)
               xyz(m,2)=xyztmpl(2,j,ls)
               xyz(m,3)=xyztmpl(3,j,ls)
            end do
         end if
      end do
      nats=m
c
c     2. Calculate ASA
c
      call solva1 (nats,xyz,rads,accs,probe,zslice)
c
c     3. Calculate solvation energy of replaced residue
c     taking into account its environmental type
c
      esolv=0.
      do i=1,natres(jres)
         num=numipos(i)
         knd=kndat(i,jres)
c
         del1=accs(num)*ASP(knd)
         del2=accs(num)*(ASP(knd)-ASP_chx(knd)) + accs(num)*ASPlip
         if(ienv(ipos).eq.1) esolv=esolv + del1
         if(ienv(ipos).eq.2) esolv=esolv + del2
      end do
c     if(ienv(ipos).eq.1) etransf=esolv - solv_hel(1,jres)
c     if(ienv(ipos).eq.2) etransf=esolv - solv_hel(2,jres)
      if(ienv(ipos).eq.1) etransf=esolv 
      if(ienv(ipos).eq.2) etransf=esolv 
c
c     4. Calculate interaction energy contributions
c
      call interactions (jres,ipos,xyzc,epair,einter,
     *  ehb_bb,ehbonds,iprint,nact,iact)
      return
      end
c
      subroutine interactions (jres,ipos,xyzc,epair,einter,
     *  ehb_bb,ehbonds,iprint,nact,iact)
c     -----------------------------------------------------
c     Calculate interaction energy (repulsions + vdW + H-bonds)
c     of residue 'jres' in template position 'ipos' with all
c     surrounding residues, given coordinates of the residue, xyzc.
c     Coordinates of all surrounding residues are fixed.
c
c     xyz -coordinates of replaced residue (only one conformer)
c
      include 'template.inc'
      include 'residues.inc'
c
      integer list(6)/-4,-3,-1,1,3,4/,iact(1),ihbb(150)
c
      real xyzc(3,1),emaxhbb(150),emaxvdw(150)
c
      do i=1,natres(jres)
         emaxhbb(i)=0.
         emaxvdw(i)=0.
         ihbb(i)=0.
      end do
c
c     1. Interactions of the residue (all its atoms 
c     including backbone) with surrounding residues
c
      epair=0.
      einter=0.
      ehb_bb=0.
      ehbonds=0.
      do i=1,natres(jres)
         if(i.eq.1) then
            iref=2
         else
            iref=ibonds(3,i,jres)
         end if
c        write (*,'(''!'',i4,7f6.2)') kndat(i,jres),
c    *     qat(i,jres),xyzc(1,i),xyzc(2,i),xyzc(3,i),
c    *     xyzc(1,iref),xyzc(2,iref),xyzc(3,iref)
c
         if(nlistsur(ipos).gt.0) then
            do j=1,nlistsur(ipos)
               ls=listsur(j,ipos)
               is=iseq(ls)
               do k=1,natres(is)
                  if(k.eq.1) then
                     kref=2
                  else
                     kref=ibonds(3,k,is)
                  end if
c
                  call e_pair (kndat(i,jres),qat(i,jres),
     *              xyzc(1,i),xyzc(2,i),xyzc(3,i),
     *              xyzc(1,iref),xyzc(2,iref),xyzc(3,iref),
     *              kndat(k,is),qat(k,is),
     *              xyztmpl(1,k,ls),xyztmpl(2,k,ls),xyztmpl(3,k,ls),
     *              xyztmpl(1,kref,ls),xyztmpl(2,kref,ls),
     *              xyztmpl(3,kref,ls),epair0,ehb,erep)
                  if(ehb.lt.epair) then
                     einter=einter+ehb+erep
                  else
                     einter=einter+epair0+erep
                  end if
                  if(indact(ls).eq.1) then
                     epair=epair+epair0
                     if(ehb.lt.emaxhbb(i)) then
                        emaxhbb(i)=ehb
                        emaxvdw(i)=epair0
                        ihbb(i)=0
                     end if    
                  end if
               end do
            end do
         end if
c        write (*,'(''!!!'',2i4,f6.2)') i,j,epair
      end do
c
      if(natsch(jres).le.1) return
c
c     determine SS segment:
c
      do i=1,nsegm
         if(ipos.ge.isegm(1,i).and.ipos.le.isegm(2,i)) ihel=i
      end do
c
c     2. Interactions of the replaced side-chain (starting from CG!)
c     with other side-chains (starting from CG!) in the same helix,
c     and hydrogen bonds with backbone of own helix:
c
      do i=4,natres(jres)-2
         iref=ibonds(3,i,jres)
c
         do j=1,6
            jsur=ipos+list(j)
            if(jsur.ge.isegm(1,ihel).and.jsur.le.isegm(2,ihel)) then
               is=iseq(jsur)
c
c              H-bonds with own helix backbone:
c
               call e_pair (kndat(i,jres),qat(i,jres),
     *           xyzc(1,i),xyzc(2,i),xyzc(3,i),
     *           xyzc(1,iref),xyzc(2,iref),xyzc(3,iref),3,0.,
     *           xyztmpl(1,1,jsur),xyztmpl(2,1,jsur),xyztmpl(3,1,jsur),
     *           xyztmpl(1,2,jsur),xyztmpl(2,2,jsur),xyztmpl(3,2,jsur),
     *           epair0,ehb,erep0)
               if(ehb.lt.emaxhbb(i)) then
                  emaxhbb(i)=ehb
                  emaxvdw(i)=epair0
                  ihbb(i)=1
               end if    
               ip=natres(is)
               call e_pair (kndat(i,jres),qat(i,jres),
     *           xyzc(1,i),xyzc(2,i),xyzc(3,i),
     *           xyzc(1,iref),xyzc(2,iref),xyzc(3,iref),4,0.,
     *           xyztmpl(1,ip,jsur),xyztmpl(2,ip,jsur),
     *           xyztmpl(3,ip,jsur),xyztmpl(1,ip-1,jsur),
     *           xyztmpl(2,ip-1,jsur),xyztmpl(3,ip-1,jsur),epair0,
     *           ehb,erep)
               if(ehb.lt.emaxhbb(i)) then
                  emaxhbb(i)=ehb
                  emaxvdw(i)=epair0
                  ihbb(i)=1
               end if    
c
c              Interactions with other side-chains from same helix:
c
c              Check that 'jsur' is active residue:
c
               do k=1,nact
                  if(jsur.eq.iact(k)) go to 20
               end do
               go to 30
   20          continue
c
               if(natsch(is).gt.1) then
                  do k=isch(1,is)+1,isch(2,is)
                     kref=ibonds(3,k,is)
                     call e_pair (kndat(i,jres),qat(i,jres),
     *                 xyzc(1,i),xyzc(2,i),xyzc(3,i),
     *                 xyzc(1,iref),xyzc(2,iref),xyzc(3,iref),
     *                 kndat(k,is),qat(k,is),
     *                 xyztmpl(1,k,jsur),xyztmpl(2,k,jsur),
     *                 xyztmpl(3,k,jsur),xyztmpl(1,kref,jsur),
     *                 xyztmpl(2,kref,jsur),xyztmpl(3,kref,jsur),
     *                 epair0,ehb,erep)
                     if(ehb.lt.epair) then
                        einter=einter+ehb+erep
                     else
                        einter=einter+epair0+erep
                     end if
                     if(indact(jsur).eq.1) then
                        epair=epair+epair0
                        if(ehb.lt.emaxhbb(i)) then
                           emaxhbb(i)=ehb
                           emaxvdw(i)=epair0
                           ihbb(i)=0
                        end if    
                     end if
                  end do
               end if
   30          continue
            end if
         end do
      end do
c
      do i=1,natres(jres)
         if(emaxhbb(i).lt.emaxvdw(i)) then
            if(ihbb(i).eq.1) then
               ehb_bb=ehb_bb+emaxhbb(i)
            else
               epair=epair+emaxhbb(i)-emaxvdw(i)
               ehbonds=ehbonds+emaxhbb(i)-emaxvdw(i)
            end if
         end if
      end do
c
      return
      end
c
      subroutine e_pair (knd1,q1,x1,y1,z1,x01,y01,z01,
     *  knd2,q2,x2,y2,z2,x02,y02,z02,epair,ehb,erep)
c     -----------------------------------------------
c     Calculate interaction energy between two atoms
c
c     Can be optimized in the future by tabulating B and C
c     Lennard-Jones coefficients and working with r2=r**2 values.
c
c     epair - energy of all pairwise interactions except
c             H-bonds and repulsions
c
      include 'parameters.inc'
c
      real req_vdW(7,7),req_rep(7,7)
c
      epair=0.
      ehb=0.
      do i=1,7
         do j=1,7
            req_vdW(i,j)=rad_vdW(i)+rad_vdW(j)
            req_rep(i,j)=rad_rep(i)+rad_rep(j)
         end do
      end do
c
      a=x2-x1
      b=y2-y1
      c=z2-z1
      r=sqrt(a*a+b*b+c*c)
c
c     H-bonds:
c
      if(ehbond(knd1,knd2).gt.0.01) then
         if(r.lt.cuthb) then
c
c           check angle criteria:
c
            call angv (x01,y01,z01,x1,y1,z1,x2,y2,z2,ang1)
            call angv (x1,y1,z1,x2,y2,z2,x02,y02,z02,ang2)
            if(ang1.gt.angcut.and.ang2.gt.angcut) then
               if(r.gt.rhbond) then
                  cf=5.*(rhbond/r)**12 - 6.*(rhbond/r)**10
               else
                  cf=(1.-r/rhbond)**2-1.
               end if
c
c              cf < 0 !
c
               ehb=cf*ehbond(knd1,knd2)
c
c**            go to 10
            end if
         end if
      end if
c
c     SS-bonds (did not use angle criteria):
c
      if(knd1.eq.7.and.knd2.eq.7) then
         if(r.lt.rdisulf) then
            epair=edisulf
            go to 20
         end if
      end if
c
c     vdW interactions:
c
      r0=req_vdW(knd1,knd2)
      if(r.gt.r0) then
         cf=(r0/r)**12 - 2.*(r0/r)**6
      else
         cf=(1.-r/r0)**2-1.
      end if
      epair=cf*evdW(knd1,knd2)
   10 continue
c
c     Charge-charge electrostatic interactions 
c     (no ASA-dependence of dielectric constant)
c     
c*    if(q1.ne.0..and.q2.ne.0.) then
c*       e=e+332.*q1*q2/(eps1*r)
c*    end if
c
c     Repulsions (for vdW and H-bond pairs):
c
      if(r.lt.req_rep(knd1,knd2)) then
         a=r - req_rep(knd1,knd2)
         erep=a*a*wrepuls
      else
         erep=0.
      end if
   20 continue
      return
      end
c
      subroutine angv (x1,y1,z1,x2,y2,z2,x3,y3,z3,ang)
c     ------------------------------------------------
      data pi/3.14159/
c
      x=x2-x1
      y=y2-y1
      z=z2-z1
      a2=x*x+y*y+z*z
c
      x=x3-x2
      y=y3-y2
      z=z3-z2
      b2=x*x+y*y+z*z
c
      x=x3-x1
      y=y3-y1
      z=z3-z1
      c2=x*x+y*y+z*z
c
      cosang=(a2+b2-c2)/(2.*sqrt(a2*b2))
      ang=(acos(cosang)/pi)*180.
      return
      end
