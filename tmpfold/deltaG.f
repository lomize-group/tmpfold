      subroutine deltaG (indsub,iprint,pdbtempl,
     *  envfile,nbord,ibord,nact,iact,eres_tot,entropy_tot,
     *  ehd,ehbonds_tot,iflex,etransfer_tot,ireturn)
c     --------------------------------------------------
c     Stability of the entire complex or a subunit - contribution
c     of active residues only
c
c     pdbtempl - intermediate PDB file (used as input here)
c     nbord - number of segments, which define subunits
c     ibord - numbers of first and last residues in the segments
c     nact  - number of active residues
c     iact  - numbers (list) of active residues (interactions between 
c             active residues within same segment are not included)
c
      include 'template.inc'
      include 'residues.inc'
c
      character*80 envfile
      character*180 pdbtempl
c
      integer ibord(2,1),iact(1)
c
      if(iprint.ge.2) then
         write (*,'(''deltaG subroutine,  subunit:'',i4)') indsub
         do i=1,nbord
            write (*,'(''borders: '',2i4)') (ibord(j,i),j=1,2)
         end do
         write (*,'(''Active residues:'')')
         write (*,'(20i4)') (iact(i),i=1,nact)
         do i=1,nsegm
            write (*,'(''SS segments :'',2i4)') (isegm(j,i),j=1,2)
         end do
         do i=1,ntm
            write (*,'(''TM segments :'',2i4)') (numtm(j,i),j=1,2)
         end do
      end if
c
c     1. Read template PDB coordinates; renumber residues; keep reference
c     PDB numbers and separate backbone coordinates; reorder atoms as in
c     library of residues, and determine transformation matrices.
c    
c     Only residues within subunit borders (nbord,ibord) will be included
c     in template (therefore, renumbering again).
c
      call read_templ (pdbtempl,iprint,nbord,ibord,ireturn)
      if(ireturn.eq.1) return
c
c     2. Renumber borders of secondary structures and active residues
c     in accordance with the continuous residue numbering in template
c     (reference numbers).  This is needed only for subunits.
c
      do i=1,nsegm
         call renumb (nres,numref,isegm(1,i))
         call renumb (nres,numref,isegm(2,i))
c        write(*,'(2i4)') isegm(1,i),isegm(2,i)
      end do
c
      if(ntm.gt.0) then
         do i=1,ntm
            call renumb (nres,numref,numtm(1,i))
            call renumb (nres,numref,numtm(2,i))
         end do
      end if
c
      do i=1,nact
         call renumb (nres,numref,iact(i))
         do j=1,nsegm
            if(iact(i).ge.isegm(1,j).and.iact(i).le.isegm(2,j)) 
     *        go to 10
         end do
         write (*,'(''Active residue'',2i4,
     *     '' was outside of all subunits'')') i,iact(i)
         stop
   10    continue
      end do
c
      if(iprint.ge.2) then
         write (*,'(''After renumbering:'')') 
         write (*,'(''Active residues:'')')
         write (*,'(20i4)') (iact(i),i=1,nact)
         do i=1,nsegm
            write (*,'(''SS segments :'',2i4)') (isegm(j,i),j=1,2)
         end do
         do i=1,ntm
            write (*,'(''TM segments :'',2i4)') (numtm(j,i),j=1,2)
         end do
      end if
c
c     3. Determine environments of residues in template,
c
c     Default - P/W: 
c
      do i=1,nres
         ienv(i)=2
      end do
c
c     Redefine P/L within TM segments:
c
      if(ntm.gt.0) then
         do i=1,ntm
            do j=numtm(1,i),numtm(2,i)
               ienv(j)=2
            end do
         end do
      end if
c
c     4. Prepare lists of surrounding residues 
c
      call lists (iprint,nact,iact)
c
c     5. Calculate helix-helix backbone electrostatic energy
c
      call heldip (ehd)
c
c     6. Calculate deltaG binding for the set of active residues
c
      etransfer_tot=0.
      eres_tot=0.
      ehbown_tot=0.
      entropy_tot=0.
      ehbonds_tot=0.
      do i=1,nact
         ipos=iact(i)
         jres=iseq(ipos)
c
c        write (*,'(''call contrib'',2i5)') ipos,jres 
         call contrib (jres,ipos,etransfer,eresres,ehbown,entropy,
     *     ehbonds,iprint,nact,iact,iflex,ireturn)
         if(ireturn.eq.1) return
c
         etransfer_tot=etransfer_tot+etransfer
c        write (*,'(''!!'',f6.2)') eresres
         eres_tot=eres_tot+eresres
         ehbown_tot=ehbown_tot+ehbown
         entropy_tot=entropy_tot+entropy
         ehbonds_tot=ehbonds_tot+ehbonds
      end do
c
      if(iprint.ge.2) then
c        esub=etransfer_tot+eres_tot+ehbown_tot+entropy_tot+ehd
         esub=eres_tot+entropy_tot+ehd
         write (*,'(''SU'',i2,'' transf:'',f6.2,'' interact:'',
     *     f6.2,''  HBown:'',f6.2,''  entropy:'',f6.2)') indsub,
     *     etransfer_tot,eres_tot,ehbown_tot,entropy_tot
         write (*,'(3f6.2)') ehd,ehbonds_tot,esub
c        write (*,'('' hel_dip.:'',f6.2,'' H-bonds:'',6.2,
c    *    '' Total:'',f6.2)') ehd,ehbonds_tot,esub
      end if
c
c     close (11)
c     if(iprint.ge.1) close (15)
      return
      end
c
      subroutine renumb (nres,numref,num)
c     -----------------------------------
c     transform reference PDB number (input 'num') into corresponding
c     consequitive number (output 'num')
c
      integer numref (1)
c   
      do i=1,nres
         if(numref(i).eq.num) then
            num=i
            go to 10
         end if
      end do
      write (*,'(''reference residue number'',i4,
     *  '' was not found'')') num
      stop
c
   10 continue
      return
      end
c
      subroutine heldip (ehd)
c     ----------------------
c     Helix-helix backbone electrostatic energy for the entire
c     complex or subunit  (only between TM segments!)
c
      include 'template.inc'
c
c          ECEPP/2-based atomic charges:
c     real qbb(6)/-0.356,0.114,0.0,0.450,-0.384,0.176/
c                    N     CA   CB    C     O     NH
c         bond dipole moment-based charges
c     real qbb(6)/-0.270,0.000,0.0,0.420,-0.420,0.270/
c               CHARMM charges:
      real qbb(6)/-0.400,0.100,0.0,0.600,-0.550,0.250/
c
      eps=4.
      ehd=0.
      if(ntm.le.1) return
c     do i=numtm(1,1),numtm(2,1)
c        do j=1,6
c           write (*,'(4f8.3)') (xmod(k,j,i),k=1,3),qbb(j)
c        end do
c     end do
c     write (*,'(''!!!'')')
c     do i=numtm(1,2),numtm(2,2)
c        do j=1,6
c           write (*,'(4f8.3)') (xmod(k,j,i),k=1,3),qbb(j)
c        end do
c     end do
      do i=1,ntm-1
         i1=i+1
         do j=i1,ntm
c*          write (*,'(''heldip:'',4i4)') numtm(1,i),numtm(2,i),
c*   *        numtm(1,j),numtm(2,j)
            n1=numtm(1,i)
            n2=numtm(2,i)
            m1=numtm(1,j)
            m2=numtm(2,j)
            do k=n1,n2
               do l=m1,m2
c
c                 interaction of residues k and l:
c
                  do m=1,6
                     do n=1,6
                        a=xmod(1,m,k)-xmod(1,n,l)
                        b=xmod(2,m,k)-xmod(2,n,l)
                        c=xmod(3,m,k)-xmod(3,n,l)
                        r=sqrt(a*a+b*b+c*c)
                        ehd=ehd + 332.*qbb(m)*qbb(n)/(eps*r)
                     end do
                  end do
               end do
            end do
         end do
      end do
      return
      end 
