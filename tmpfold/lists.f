      subroutine lists (iprint,nact,iact)
c     -----------------------------------
c     Prepare lists of surrounding atoms and residues for
c     calculation of substitution energies in each template
c     position (only within SS segments).
c
c     listres  - list of residues (i and all surrounding residues)
c                for calculations of ASA
c     nlistres - number of residues in this list
c
c     listsur  - list of residues surrounding i, excluding own helix
c                for calculations of Eij (vdW, H-bonds, and repulsions)
c     nlistsur - number of residues in this list
c
      include 'template.inc'
      include 'residues.inc'
c
      integer iact(1),ibord(2,1)
c
      data distcut /7.0/
c
c     distcut - distance cutoff to determine adjacent residues
c
      if(iprint.gt.2) write (*,'(''lists:'')')
c
c     Indices "active/not active" for all template residues:
c 
      do i=1,nres
         indact(i)=0
      end do
      do k=1,nact
         indact(iact(k))=1
      end do
c
c     Determine lists of all surrounding residues for
c     calculations of ASA (listres) and Eij (listsur)
c
      do i=1,nres
         m1=0
         m2=0
         do j=1,nres
            call contact (i,j,distcut,icont)
            if(icont.eq.1) then
               m1=m1+1
               if(m1.gt.maxlist_res) then
                  write (*,'(''Too many active residues'')') 
                  stop
               end if
               listres(m1,i)=j
c
c              check that residues i and j do not belong to same
c              secondary structure:
c
               if(j.eq.i) go to 10
               do k=1,nsegm
                  if(i.ge.isegm(1,k).and.i.le.isegm(2,k).and.
     *               j.ge.isegm(1,k).and.j.le.isegm(2,k)) go to 10
               end do
               m2=m2+1
               listsur(m2,i)=j
   10          continue
            end if
         end do
         nlistres(i)=m1 
         nlistsur(i)=m2 
c  
c        m2 can be zero, for example, in a single helix
c
         if(iprint.gt.2) then
            write (*,'(20i4)') i,(listres(l,i),l=1,m1)
            if(m2.eq.0) then
               write (*,'(''-'',20i4)') i,indact(i),m2
            else
               write (*,'(''-'',20i4)') i,indact(i),
     *           (listsur(l,i),l=1,m2)
            end if
         end if
      end do
      return
      end
c
      subroutine contact (i,j,distcut,icont)
c     ---------------------------------------
      include 'template.inc'
      include 'residues.inc'
c
      icont=0
      is=iseq(i)
      js=iseq(j)
      do k=1,natres(is)
         do l=1,natres(js)
            x=xyztmpl(1,k,i)-xyztmpl(1,l,j)
            y=xyztmpl(2,k,i)-xyztmpl(2,l,j)
            z=xyztmpl(3,k,i)-xyztmpl(3,l,j)
            r=sqrt(x*x+y*y+z*z)
            if(r.lt.distcut) then
               icont=1
               go to 20
            end if
         end do
      end do
   20 continue
      return
      end
