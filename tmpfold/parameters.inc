c                       parameters.inc
c                       --------------
c    ASP       - ASP for water->protein transfer
c    ASP_chx   - ASP for water->cyclohexane transfer
c    rad_vdW   - equilibrium intertomic distances for modified
c                6-12 potentials (from ECEPP/2)
c    rad_asa   - vdW radii (Chothia) for calculation of ASA
c    rad_rep   - vdW radii for calculation of repulsion energy
c    evdW      - equilibrium energies of modified 6-12 potentials
c    ehbond    - equilibrium energies of modified 10-12 potentials
c    wrepuls - weight of repulsion energy
c    rhbond  - equilibrium distance for all H-bonds
c    cuthb   - distance cutoff for H-bonds
c    angcut  - angle cutoff for H-bonds
c    qcoef   - coefficient defining the increase of H-bond energy
c              for charged groups
c
c     Atom types:
c     1  - C aliphatic
c     2  - C, aromatic and carbonyl
c     3  - N
c     4  - =O, carbonyl
c     5  - -OH, hydroxyl
c     6  - S of Met
c     7  - SH of Cys
c
      data zslice/0.05/,probe/1.4/,rhbond/2.9/,angcut/80./,cuthb/4.0/,
     *  rdisulf/2.5/,edisulf/-2.0/
c
      real ASP(7)/0.019,0.007,-0.021,-0.066,-0.066,2*-0.001/,
c    * ASP_chx(7)/0.025,0.017,-0.060,-0.065,-0.065,2*-0.014/,
     * ASP_chx(7)/0.025,0.019,-0.055,-0.063,-0.063,2*-0.013/,
     * ASPlip/0.0/,
     *  rad_vdW(7)/2.06,1.86,1.76,1.58,1.58,2*2.07/,
     *  rad_asa(7)/1.87,1.76,1.65,1.40,1.40,2*1.85/,
     *  rad_rep(7)/1.77,1.66,1.55,1.30,1.30,2*1.75/,
c    *  rad_rep(7)/1.40,1.40,1.30,1.20,1.20,2*1.60/,
     *  evdW (7,7)/ 0.062, 0.021,-0.013,-0.013,-0.013, 2*0.064,
     *              0.021, 0.119,-0.013,-0.013,-0.013, 2*0.082,
     *             -0.013,-0.013, 0.342, 0.342, 0.342, 2*0.104,
     *             -0.013,-0.013, 0.342, 0.342, 0.342, 2*0.104,
     *             -0.013,-0.013, 0.342, 0.342, 0.342, 2*0.104,
     *              0.064, 0.082, 0.104, 0.104, 0.104, 2*0.200,
     *              0.064, 0.082, 0.104, 0.104, 0.104, 2*0.200/,
     *  ehbond(7,7)/ 0.00, 0.00, 0.00, 0.00, 0.00, 2*0.00,
     *               0.00, 0.00, 0.00, 0.00, 0.00, 2*0.00,
     *               0.00, 0.00, 0.00, 1.49, 1.84, 2*0.00,
     *               0.00, 0.00, 1.49, 0.00, 1.62, 2*0.00,
     *               0.00, 0.00, 1.84, 1.62, 1.84, 2*0.00,
     *               0.00, 0.00, 0.00, 0.00, 0.00, 2*0.00,
     *               0.00, 0.00, 0.00, 0.00, 0.00, 2*0.00/,
     *  charge(5)/-0.50,-0.49,1.00,0.33,0.50/, 
     *  ASP_charge(5)/-0.070,-0.049,-0.106,-0.041,-0.029/,
     *  ASP_chx_charge(5)/-0.070,-0.049,-0.106,-0.071,-0.029/
c
       common /inpar/ wrepuls
