      subroutine read_res (reslib1,iprint)
c     -----------------------------------
c          Read parameters of residues
c
c                      Residues:
c     ndict      - number of residues in the library
c     namres_full- 4-letter names of residues
c     namres     - 1-letter names of residues
c     prop_hel   - helix propensities, middle positions
c     prop_helc  - helix propensities, C-turn positions
c     prop_ncap  - helix propensities, N-cap positions
c     prop_bet   - beta-sheet propensidies, middle positions
c     prop_edge  - beta-sheet propensities, edge strand
c     prop_coil  - beta-sheet propensities, edge strand
c     pho_hprot  - water/protein transfer energies, alpha-helix
c     pho_hchx   - water/cyclohexan transfer energies, alpha-helix
c     pho_bet    - water/protein transfer energies, beta-sheet
c     entr_hel   - side-chain conformational entropies in alpha-helix
c     entr_bet   - side-chain conformational entropies in beta-sheet
c     entr_coil  - side-chain conformational entropies in coil
c     isch       - numbers of 1st (CB) and last atoms of side-chain
c     nchi       - number of chi angles (from 0 to 4)
c     natsch     - number of atoms in side-chain
c     asa_ref    - reference total ASA from NACCESS
c     asa_hel    - reference total ASA in helix
c     solv_hel   - reference solvation energies of residues in helix
c     solv_bet   - reference solvation energies of residues in beta-sheet
c                  (include those for beta-sheet edge residues later)
c     solv_coil  - reference solvation energies of residues in coil
c
c                      Atoms:
c     natres     - number of atoms in residues
c     kndat      - types of atoms
c     namat      - names of atoms
c     qat        - charges of atoms
c     ibonds     - connectivities of atoms
c     bonds      - bond lengths for calc. of atom coordinates
c     valangs    - valent angles for calc. of atom coordinates
c     dihangs    - dihedral angles for calc. of atom coordinates
c
c                  Torsion angles:
c     ntors      - number of torsion angles in residues
c     ktors      - numbers of atoms forming torsion angles
c     ltors      - periodicity of torsion potential
c     etors      - torsion energy barriers
c     namtors    - names of torsion angles
c
c     solv       - atomic solvation parameters
c
c     Currently lacking: coil reference energies; reference ASA
c     and conformational entropies in beta-sheet and coil
c
      include 'residues.inc'
      include 'parameters.inc'
c
      character*80 reslib1
c
c     real solv(6)/0.019,0.007,-0.001,-0.066,-0.066,-0.021/
c
      open (2,file=reslib1)
      read (2,'(i3)') ndict
      if(ndict.gt.max_res) then
         write (*,'(''Too many residues in dictionary'')')
         stop
      end if
      do i=1,ndict
         read (2,'(1x)')
         read (2,'(1x,a4,2a1,2i3)'),namres_full(i),
     *     (namres(j,i),j=1,2),ntors(i),natres(i)
         if(natres(i).gt.maxat_res) then
            write (*,'(''Too many atoms in residue '',a1)') 
     *        namres(1,i)
            stop
         end if
         if(ntors(i).gt.maxtors) then
            write (*,'(''Too many torsions in residue '',a1)') 
     *        namres(1,i)
            stop
         end if
c
         read (2,'(5f6.2)') prop_hel(i),prop_helc(i),prop_ncap(i),
     *     prop_bet(i),prop_edge(i)
         read (2,'(5f6.2)') pho_hprot(i),pho_hchx(i),pho_bet(i)
         read (2,'(5f6.2)') entr_hel(i)
         read (2,'(f5.0)') asa_ref(i)
c
         do j=1,ntors(i)
            read (2,'(3i3,f4.1,2x,a4)') (ktors(l,j,i),l=1,2),
     *        ltors(j,i),etors(j,i),namtors(j,i)
         end do
c
         solv_hel(1,i)=0.
         solv_hel(2,i)=0.
         solv_bet(i)=0.
         solv_coil(i)=0.
         asa_hel(i)=0.
         do j=1,natres(i)
            read (2,'(1x,a4,i2,2i3,f6.3,f6.1,f8.2,i3,f6.2,f4.0)')
     *        namat(j,i),(ibonds(l,j,i),l=1,3),bonds(j,i),
     *        valangs(j,i),dihangs(j,i),kndat(j,i),qat(j,i),asahel
            solv_hel(1,i) = solv_hel(1,i) + ASP(kndat(j,i))*asahel
            solv_hel(2,i) = solv_hel(2,i) +(ASP(kndat(j,i))-
     *        ASP_chx(kndat(j,i)))*asahel + ASPlip*asahel
            asa_hel(i)=asa_hel(i) + asahel
         end do
c
c        determine 1st and last atoms of side-chain:
c
         isch(1,i)=0
         do j=1,natres(i)
            if(namat(j,i).eq.'CB  ') isch(1,i)=j
            if(namat(j,i).eq.'C   ') isch(2,i)=j-1
         end do
c
         if(isch(1,i).eq.0) then
            natsch(i)=0
         else
            natsch(i)=isch(2,i)-isch(1,i)+1
         end if
c
         nchi(i)=0
         do j=1,ntors(i)
            if(namtors(j,i)(1:3).eq.'chi') nchi(i)=nchi(i)+1
         end do
      end do
c
c
      if(iprint.gt.2) then   
         write (*,'(''read_res:'')')
         do i=1,ndict
            write(*,'(1x,a4,2a1,2i3)'),namres_full(i),
     *        (namres(j,i),j=1,2),ntors(i),natres(i)
            write (*,'(9f6.2)') prop_hel(i),prop_helc(i),prop_ncap(i),
     *        prop_bet(i),prop_edge(i),solv_hel(1,i),solv_hel(2,i),
     *        solv_bet(i),solv_coil(i) 
            write (*,'(5f6.2)') pho_hprot(i),pho_hchx(i),pho_bet(i)
            write (*,'(5f6.2)') entr_hel(i),asa_ref(i)
            do j=1,ntors(i)
               write(*,'(3i3,f4.1,2x,a4)') (ktors(l,j,i),l=1,2),
     *           ltors(j,i),etors(j,i),namtors(j,i)
            end do
            do j=1,natres(i)
               write(*,'(1x,a4,i2,2i3,f6.3,f6.1,f8.2,i3,f6.2)')
     *           namat(j,i),(ibonds(l,j,i),l=1,3),bonds(j,i),
     *           valangs(j,i),dihangs(j,i),kndat(j,i),qat(j,i)
            end do
            write(*,'(4i4)') isch(1,i),isch(2,i),nchi(i),natsch(i)
            write(*,'(1x)')
         end do
      end if
c        
      close (2)
      return
      end 
c
      subroutine read_rota (rotlib,iprint)
c     -----------------------------------
c     Read chi angles and xyz coordinates of side-chain atoms
c     from rotamer library 
c
c     The side-chain coordinates will be defined for all 
c     residues except Gly
c
c     rothel.lib - updated version for alpha-helices; it might be reduced
c
c     Keep only side-chain nonhydrogen coordinates (starting from CB) 
c     for conformers, because backbone coordinates will be taken 
c     from template 
c
      parameter (maxbuf=100)
c
      include 'residues.inc'
c
c     max_res   - maximal number of residues in the library
c     nchi       - number of chi angles (from 0 to 4)
c     maxcnf_res- maximal number of conformers in residue
c     nconf      - number of conformers in residues
c                   Output:
c     xyz_conf   - coordinates of conformers
c     chi_conf   - chi angles
c
      character*80 line,rotlib
      character*4 namcur,namatbuf(maxbuf)
c
      integer ichi(4)
c
      real xyzbuf(3,maxbuf)
c
      open (8,file=rotlib)
      if(iprint.gt.2) write (*,'(''read_rota:'')')
      do i=1,max_res
         read (8,'(a4,2i3)',end=20) namcur,ncnf,nat
         if(ncnf.gt.maxcnf_res) then
            write (*,'(''Too many conformations for '',a4,
     *        '' in rotamer library'')') namcur
            stop
         end if
         if(nat.gt.maxbuf) then
            write (*,'(''Too many atoms in '',a4,
     *        '' in rotamer library'')') namcur
            stop
         end if
         if(iprint.gt.2) then
            write(*,'('' '')') 
            write(*,'(a4,2i3)') namcur,ncnf,nat
         end if
c
c        find corresponding residue from res.lib:
c
         do j=1,ndict
            if(namres_full(j).eq.namcur) go to 10
         end do
         write (*,'(''Residue '',a4,
     *     ''was not found in the list of residues'')') namcur
         stop
c
   10    continue
         ires=j
         nconf(ires)=ncnf
         do k=1,nconf(ires)
            read (8,'(4i4)',end=20) (ichi(l),l=1,4)
            if(nchi(ires).gt.0) then
               do l=1,nchi(ires)
                  chi_conf(l,k,ires)=float(ichi(l))
               end do
               if(iprint.gt.2) write (*,'(5f7.0)')
     *           (chi_conf(ll,k,ires),ll=1,nchi(ires))
            end if
            do l=1,nat
               read (8,'(a)') line
               read (line,'(13x,a4,13x,3f8.3)') namatbuf(l),
     *           (xyzbuf(m,l),m=1,3)
            end do
c
c           order side-chain atoms as in residue library
c           (xyz_conf store only side-chain coordinates)
c
            if(natsch(ires).ge.1) then
               m=0
               do l=isch(1,ires),isch(2,ires)
                  do n=1,nat
                     if(namatbuf(n).eq.namat(l,ires)) go to 15
                  end do
                  write (*,'(''atom'',2(1x,a4),'' not found'',
     *              '' in rotamer library'')') namat(l,ires),namcur
                  stop
c
   15             continue
                  m=m+1
                  xyz_conf(1,m,k,ires)=xyzbuf(1,n)
                  xyz_conf(2,m,k,ires)=xyzbuf(2,n)
                  xyz_conf(3,m,k,ires)=xyzbuf(3,n)
c
c*                if(nchi(ires).eq.0) write (*,'(a4)') namres_full(ires)
                  if(iprint.gt.2) write(*,'(a4,1x,3f8.3)') 
     *              namat(l,ires),(xyz_conf(ll,m,k,ires),ll=1,3)
               end do
            end if
         end do
      end do
c
   20 continue
      close (8)
      return
      end
