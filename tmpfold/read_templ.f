      subroutine read_templ (pdbtempl,iprint,nbord,ibord,ireturn)
c     ---------------------------------------------------
c     Read template PDB coordinates; renumber residues; keep
c     reference PDB numbers and separate backbone coordinates;
c     reorder atoms as in reference library, and determine
c     transformation matrices.
c
c     All subunit residues are included here; not only from
c     secondary structures.
c
c     Use N, CA, CB, and C' as reference atoms for superposition,
c     and reconstruct "dummy" CB-atom for Gly in template.
c     Coordinates of target glycines are copied from template
c     backbone
c
c     pdbtempl - template PDB file
c
c     maxres   - maximal number of residues in template
c     maxat_all- maximal number of ALL atoms in residue
c
c     nres     - number of residues in template, including residues
c                between SS segments
c     numref   - reference PDB numbers
c     seq      - amino acid sequence of template
c
c     vect     - translation vector for each position in template
c                to calculate replaced side-chain coordinates
c     rot      - rotational transformation matrix for each position
c     xyztmpl  - coordinates of all template atoms
c
c                     Internal arrays:
c     xmod     - backbone coordinates to calculate rotation matrices
c     xyzpdb   - original PDB coordinates of all atoms
c     namatpdb - original names of atoms in PDB (character*5)
c     natres_tmpl - numbers of atoms in residues of PDB template,
c                   including hydrogen atoms
c
      include 'template.inc'
      include 'residues.inc'
c
      parameter (maxat_all=50)
c
      character*80 line
      character*180 pdbtempl
      character*5 namatpdb(maxat_all,maxres),namat_cur
      character*4 bblist(5)/' N  ',' CA ',' CB ',' C  ',' O  '/,
     *  namres_cur
c
      real xyzpdb(3,maxat_all,maxres),x1(3),x2(3),x3(3),x4(3)
c
      integer natres_tmpl(maxres),ibord(2,1)
      character*1 zero/' '/
c
c     1. read PDB coordinates of the model structure,
c     including only residues from the corresponding subunit
c     (all PDB residues are renumbered sequentially here)
c
      ireturn=0
      open (3,file=pdbtempl)
c
      ires_cur=0
      numres0=-999
      do i=1,20000
         read (3,'(a)',end=10) line
         if(line(1:4).eq.'ATOM') then
            if(line(17:17).eq.' '.or.line(17:17).eq.'A') then
            if(line(17:17).eq.'A') line(17:17)=zero
            read (line,'(12x,a5,a4,1x,i4,4x,3f8.3)') 
     *         namat_cur,namres_cur,numres_cur,xc,yc,zc
c
            do k=1,nbord
               if(numres_cur.ge.ibord(1,k).and.
     *            numres_cur.le.ibord(2,k)) go to 5
            end do
            go to 7
    5       continue
            if(numres_cur.eq.numres0) then
               iat=iat+1
            else
               numres0=numres_cur
               if(ires_cur.gt.0) natres_tmpl(ires_cur)=iat
               iat=1
               ires_cur=ires_cur+1
               numref(ires_cur)=numres_cur
               seq(ires_cur)=namres_cur
            end if
            namatpdb(iat,ires_cur)=namat_cur
            xyzpdb(1,iat,ires_cur)=xc
            xyzpdb(2,iat,ires_cur)=yc
            xyzpdb(3,iat,ires_cur)=zc
    7       continue
            end if
         end if
      end do
   10 continue
      natres_tmpl(ires_cur)=iat
      nres=ires_cur
c
c     2. Determine backbone coordinates and 'iseq' addresses and 
c     reorder atoms as in residue library
c
      do i=1,maxres
         do j=1,3
            do k=1,6
               xmod(j,k,i)=0.
            end do
         end do
      end do
c
      do i=1,nres
c
c        determine xmod, i.e. reference backbone coordinates
c        of template
c
         do j=1,natres_tmpl(i)
            do k=1,5
               if(namatpdb(j,i)(1:4).eq.bblist(k)) then
                  xmod(1,k,i)=xyzpdb(1,j,i)
                  xmod(2,k,i)=xyzpdb(2,j,i)
                  xmod(3,k,i)=xyzpdb(3,j,i)
               end if
            end do
         end do
c
c        reconstruct coordinates of "dummy" CB-atom for Gly
c        from its N, C', and CA atom coordinates
c
         if(seq(i).eq.'GLY ') then
            do l=1,3
               x1(l)=xmod(l,1,i)
               x2(l)=xmod(l,4,i)
               x3(l)=xmod(l,2,i)
            end do
            call reconstr (x1,x2,x3,x4,1.53,1.53,110.,120.)
c           write (15,'(''ATOM'',i7,''  CB  GLY '',1x,i4,4x,3f8.3)')
c    *        i,i,(x4(l),l=1,3)
            xmod(1,3,i)=x4(1)
            xmod(2,3,i)=x4(2)
            xmod(3,3,i)=x4(3)
         end if
c
c        Reconstruct coordinates of backbone NH
c
         if(i.eq.1) then
            xmod(1,6,i)=xmod(1,1,i)
            xmod(2,6,i)=xmod(2,1,i)
            xmod(3,6,i)=xmod(3,1,i)
         else
            a=xmod(1,1,i)-xmod(1,4,i-1)
            b=xmod(2,1,i)-xmod(2,4,i-1)
            c=xmod(3,1,i)-xmod(3,4,i-1)
            r=sqrt(a*a+b*b+c*c)
            if(r.gt.2.) then
               xmod(1,6,i)=xmod(1,1,i)
               xmod(2,6,i)=xmod(2,1,i)
               xmod(3,6,i)=xmod(3,1,i)
               go to 15
            end if
            if(numref(i-1).eq.numref(i)-1) then
               do l=1,3
                  x1(l)=xmod(l,2,i-1)
                  x2(l)=xmod(l,4,i-1)
                  x3(l)=xmod(l,1,i)
               end do
               call reconstr (x1,x2,x3,x4,1.325,1.0,124.,0.)
c              write (15,'(''ATOM'',i7,''  NH  XXX '',1x,i4,4x,
c    *           3f8.3)')i,i,(x4(l),l=1,3)
               xmod(1,6,i)=x4(1)
               xmod(2,6,i)=x4(2)
               xmod(3,6,i)=x4(3)
            else
               xmod(1,6,i)=xmod(1,1,i)
               xmod(2,6,i)=xmod(2,1,i)
               xmod(3,6,i)=xmod(3,1,i)
            end if
   15       continue
         end if
c
c        find corresponding residue from res.lib:
c
         do j=1,ndict
            if(namres_full(j).eq.seq(i)) go to 20
         end do
         write (*,'(''Residue '',a4,i4,'' from PDB file '',
     *     ''was not found in the library of residues'')') seq(i),i
         stop
   20    continue
         iseq(i)=j
         ires=iseq(i)
c
c        reorder atoms as in residue library
c
         do l=1,natres(ires)
            do n=1,natres_tmpl(i)
               if(namatpdb(n,i)(2:5).eq.namat(l,ires)) go to 30
            end do
            if(namat(l,ires).eq.'CG  '.or.namat(l,ires).eq.'CG1 '.or.
     *         namat(l,ires).eq.'CG  '.or.
     *         namat(l,ires).eq.'OG  '.or.namat(l,ires).eq.'OG1 ') then
               write (*,'(''atom '',a4,'' was not found'',
     *           '' for residue '',a4,i4)') 
     *           namat(l,ires),seq(i),numref(i)
               write(*,'(''Please reconstruct non-hydrogen atoms ''
     *           ''for residues in TM segments in input PDB file'')')
               stop
            else
               write (*,'(''atom '',a4,'' was not found'',
     *           '' in PDB file residue '',a4,i4)') 
     *           namat(l,ires),seq(i),numref(i)
            end if
c           ireturn=1
c           close (3)
c           return
c           stop
            go to 40
c
   30       continue
            xyztmpl(1,l,i)=xyzpdb(1,n,i)
            xyztmpl(2,l,i)=xyzpdb(2,n,i)
            xyztmpl(3,l,i)=xyzpdb(3,n,i)
   40       continue
         end do
      end do
c
      if(iprint.gt.2) then
      write (*,'(''read_templ:'')')
         do i=1,nres
            write (*,'(a4,3i4)') seq(i),i,iseq(i),numref(i)
            ires=iseq(i)
            do j=1,natres(ires)
               write (*,'(a4,2x,3f8.3)') namat(j,ires),
     *           (xyztmpl(l,j,i),l=1,3)
            end do
         end do
c
         do i=1,nres
            do j=1,6
               write (*,'(1x,i4,3f8.3)') i,(xmod(k,j,i),k=1,3)
            end do
         end do
      end if
c
c     3. Calculate translational vector vect(3,i) and rotational
c     matrix rot(3,3,i) for each position i in the template with 
c     backbone coordinates xmod(3,6,i)
c
      call trsmat (nres,xmod,vect,rot,v1)
c     do i=1,nres
c        write (*,'(i4)') i
c        do l=1,3
c           write (*,'(3f8.4)') (rot(l,k,i),k=1,3)
c        end do
c     end do
c
      close (3)
      return
      end
c
      subroutine trsmat (nresf,xmod,v,rot,v1)
c     --------------------------------------
c     Calc. of coordinate translational vector v(3,i) and rotational
c     matrix rot(3,3,i) for each position i in the template with 
c     backbone coordinates xmod(3,6,i)
c
      real xref(3,4)/3*0.,1.458,2*0.,1.935,-0.749,-1.245,2.021,1.423,
     *  0./,v(3,1),rot(3,3,1),xmod(3,6,1),v1(3),v2(3),rot0(3,3),
     *  ac(4),bc(4),cc(4),xc(4),yc(4),zc(4)
c
      do j=1,3
         v1(j)=0.
      end do
      do i=1,4
         do j=1,3
            v1(j)=v1(j)+xref(j,i)
         end do
      end do
      do j=1,3
         v1(j)=v1(j)/4.
      end do
c
      do i=1,nresf
         if(xmod(1,3,i).ne.0..or.xmod(2,3,i).ne.0..or.
     *     xmod(3,3,i).ne.0.) then
            do j=1,3
               v2(j)=0.
            end do
            do k=1,4
               do j=1,3
                  v2(j)=v2(j)+xmod(j,k,i)
               end do
            end do
            do j=1,3
               v2(j)=v2(j)/4.
            end do
c           do j=1,3
c              v(j,i)=v2(j)-v1(j)
c           end do
            do j=1,3
               v(j,i)=v2(j)
            end do
            do k=1,4
               xc(k)=xref(1,k)-v1(1)
               yc(k)=xref(2,k)-v1(2)
               zc(k)=xref(3,k)-v1(3)
               ac(k)=xmod(1,k,i)-v2(1)
               bc(k)=xmod(2,k,i)-v2(2)
               cc(k)=xmod(3,k,i)-v2(3)
c              write(*,'(6f8.3)') xc(k),yc(k),zc(k),ac(k),bc(k),cc(k)
            end do
c
            call cmpcor(4,xc,yc,zc,ac,bc,cc,0,rmsd,rot0,ier)
c
            if(ier.eq.-1) write(*,'(''fit is undefined'',i4)'),i
            if(ier.eq.-2) write(*,'(''sqrt from neg rmsd'',i4)'),i
            if(ier.gt.0) write(*,'(''rot matr is not unique'',i4)'),i
            if(rmsd.gt.0.2) write(*,'(''rmsd='',f5.1,i4)') rmsd,i
            do k=1,3
               do l=1,3
                  rot(l,k,i)=rot0(l,k)
               end do
            end do
         end if
      end do
      return
      end
c
      subroutine reconstr (x1,x2,x3,x4,r3,r4,val,tors)
c     ------------------------------------------------
c     Calculate coordinates of atom x4 from the reference 
c     coordinates of atoms x1, x2, and x3
c     (atoms x3 and x4 are connected)
c
c     r3   - bond length of last reference atom x3
c     r4   - bond length of atom x4
c     val  - valent angle of atom x4
c     tors - torsion angle of atom x4
c
      data pi/3.14159/
c
      real x1(1),x2(1),x3(1),x4(3),
     * v(3),v1(3),v2(3),vx(3),vt(3)
c
      val1=(val/180.)*pi
      tors1=(tors/180.)*pi
c
      do l=1,3 
         v1(l)=x1(l)-x2(l)
         v2(l)=x3(l)-x2(l)
         vx(l)=x3(l)-(r4/r3)*v2(l)
         vt(l)=x3(l)
      end do
      v(1)=v1(2)*v2(3)-v1(3)*v2(2)
      v(2)=v1(3)*v2(1)-v1(1)*v2(3)
      v(3)=v1(1)*v2(2)-v1(2)*v2(1)
c
      call axrot2 (vx,val1,v,vt)
      call axrot2 (vx,tors1,v2,vt)
c
      x4(1)=vx(1)
      x4(2)=vx(2)
      x4(3)=vx(3)
      return
      end
c
      SUBROUTINE AXROT2 (X,FI,V,VT)
C     ---------------------------
      REAL X(3),V(3),VT(3),H1(3),H2(3),H3(3),VB(3)
C
C     rotate point X(3) around axis V(3) that crosses point VT(3)
C
      R=SQRT(V(1)*V(1)+V(2)*V(2)+V(3)*V(3))
C
      FF=0.5*FI
      A=COS(FF)
      P=SIN(FF)/R
      B=P*V(1)
      C=P*V(2)
      D=P*V(3)
      AA=A*A
      BB=B*B
      CC=C*C
      DD=D*D
      BC=B*C*2.
      AD1=A*D*2.
      BD1=B*D*2.
      AC=A*C*2.
      CD=C*D*2.
      AB=A*B*2.
      H1(1)=AA+BB-CC-DD
      H2(1)=BC-AD1
      H3(1)=BD1+AC
      H1(2)=BC+AD1
      H2(2)=AA-BB+CC-DD
      H3(2)=CD-AB
      H1(3)=BD1-AC
      H2(3)=CD+AB
      H3(3)=AA-BB-CC+DD
C
C     torsion
C
      DO I=1,3
        VB(I)=VT(I) + H1(I)*(X(1)-VT(1)) + H2(I)*(X(2)-VT(2)) +
     *                H3(I)*(X(3)-VT(3))
      end do  
C
      DO I=1,3
        X(I)=VB(I)
      end do  
      RETURN
      END
