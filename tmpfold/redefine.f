      subroutine redefine (iprint,nsub,numsub,pdbtempl,pdbout,
     *  subunit1,subunit2,numtm1,numtm2,ireturn,idiss,nhelix,
     *  num1,num2,subunit,nnhelix,nnum1,nnum2,ii1,jj1,nnsub)
c     --------------------------------------------------------
c     Read and renumber PDB file and borders of subunits
c     and secondary structures
c
c                  Output parameters 
c                  -----------------
c     nsub, numsub  - number of subunits and their borders
c     nsegm, isegm  - number of SS segments and their borders
c     ntm, numtm    - number of transmembrane segments and their borders
c     (all of them are defined using new continuous numbering of
c     residues for the entire complex;  SS and TM segments belonging
c     to each subunit will be defined later)
c
c            Internal parameters (from SS file)
c            ----------------------------------
c     nsub_su  - number of PDB subunits in a structural subunit
c     namsub_su - ID of PDB subunits in a structural subunit
c     numsub_su - first and last residues of PDB subunits
c                 in a structural subunit
c     max_su    - maximal number of PDB subunits within one
c                 structural subunit
c     Final segments of structural subunit "i" correspond
c     to [numsub_su(1,1,i); numsub_su(2,nsub_su(i),i)]
c
c     Parameters nhelix,num1,num2,subunit,nnhelix,nnum1,nnum2,ii1,jj1
c     are used or not depending on value of idiss
c
      parameter (maxsub=25,max_su=12)
c
      include 'template.inc'
c
      character*180 pdbtempl,pdbout
      character*80 line
c
      character*1 namsub_su(max_su,maxsub),namsegm(maxsegm),
     *  namtm(maxsegm),namu,subunit1,subunit2,subunit(1),namu0
c
      integer numsub(2,1),nsub_su(maxsub),numsub_su(2,max_su,maxsub),
     *  isegm_buf(2,maxsegm),numtm_buf(2,maxsegm),numtm1(2),numtm2(2),
     *  num1(1),num2(1),nnum1(100,1),nnum2(100,1),nnhelix(1)
c
c     1. Define borders of subunits, secondary structures, and
c     transmembrane segments for the entire complex from .cor file.
c
      ireturn=0
      open (21,file=pdbtempl)
      open (22,file=pdbout)  
c
      if(idiss.eq.1) then
c
c        association energy of two helices
c
         nsub=2
         nsub_su(1)=1
         nsub_su(2)=1
         namsub_su(1,1)=subunit1
         namsub_su(1,2)=subunit2
c        numsub_su(1,1,1)=numtm1(1)-1
c        numsub_su(2,1,1)=numtm2(1)+1
c        numsub_su(1,1,2)=numtm1(2)-1
c        numsub_su(2,1,2)=numtm2(2)+1
         numsub_su(1,1,1)=numtm1(1)
         numsub_su(2,1,1)=numtm2(1)
         numsub_su(1,1,2)=numtm1(2)
         numsub_su(2,1,2)=numtm2(2)
c
         nsegm=2
         namsegm(1)=subunit1
         namsegm(2)=subunit2
c        isegm(1,1)=numtm1(1)-3
c        isegm(2,1)=numtm2(1)+3
c        isegm(1,2)=numtm1(2)-3
c        isegm(2,2)=numtm2(2)+3
         isegm(1,1)=numtm1(1)
         isegm(2,1)=numtm2(1)
         isegm(1,2)=numtm1(2)
         isegm(2,2)=numtm2(2)
         isegm_buf(1,1)=0
         isegm_buf(1,2)=0
         isegm_buf(2,1)=0
         isegm_buf(2,2)=0
c
         ntm=2
         namtm(1)=subunit1
         namtm(2)=subunit2
         numtm(1,1)=numtm1(1)
         numtm(2,1)=numtm2(1)
         numtm(1,2)=numtm1(2)
         numtm(2,2)=numtm2(2)
         numtm_buf(1,1)=0
         numtm_buf(1,2)=0
         numtm_buf(2,1)=0
         numtm_buf(2,2)=0
      end if
c
      if(idiss.eq.2) then
c
c        association energy of all helices from a single subunit
c
         nsub=nhelix
         nsegm=nhelix
         ntm=nhelix
c        write (*,'(i10)') nhelix
         do i=1,nhelix
            nsub_su(i)=1
            namsub_su(1,i)=subunit1
            numsub_su(1,1,i)=num1(i)
            numsub_su(2,1,i)=num2(i)
            namsegm(i)=subunit1
            isegm(1,i)=num1(i)
            isegm(2,i)=num2(i)
            isegm_buf(1,i)=0
            isegm_buf(2,i)=0
            namtm(i)=subunit1
            numtm(1,i)=num1(i)
            numtm(2,i)=num2(i)
            numtm_buf(1,i)=0
            numtm_buf(2,i)=0
c           write (*,'(i4,1x,a1,2i4,a1,2i4,1x,a1,2i4)') 
c    *        nsub_su(i),namsub_su(1,i),numsub_su(1,1,i),
c    *        numsub_su(2,1,i),namsegm(i),isegm(1,i),isegm(2,i),
c    *        namtm(i),numtm(1,i),numtm(2,i)
         end do
      end if
c
      if(idiss.eq.3) then
c
c        association energy of two PDB subunits
c
         nsub=2
         nsub_su(1)=1
         nsub_su(2)=1
         namsub_su(1,1)=subunit(ii1)
         namsub_su(1,2)=subunit(jj1)
         numsub_su(1,1,1)=nnum1(ii1,1)
         numsub_su(2,1,1)=nnum2(ii1,nnhelix(ii1))
         numsub_su(1,1,2)=nnum1(jj1,1)
         numsub_su(2,1,2)=nnum2(jj1,nnhelix(jj1))
c
         m=0
         do j=1,nnhelix(ii1)
            m=m+1
            namsegm(m)=subunit(ii1)
            isegm(1,m)=nnum1(ii1,j)
            isegm(2,m)=nnum2(ii1,j)
            isegm_buf(1,m)=0
            isegm_buf(2,m)=0
            namtm(m)=subunit(ii1)
            numtm(1,m)=nnum1(ii1,j)
            numtm(2,m)=nnum2(ii1,j)
            numtm_buf(1,m)=0
            numtm_buf(2,m)=0
         end do
         do j=1,nnhelix(jj1)
            m=m+1
            namsegm(m)=subunit(jj1)
            isegm(1,m)=nnum1(jj1,j)
            isegm(2,m)=nnum2(jj1,j)
            isegm_buf(1,m)=0
            isegm_buf(2,m)=0
            namtm(m)=subunit(jj1)
            numtm(1,m)=nnum1(jj1,j)
            numtm(2,m)=nnum2(jj1,j)
            numtm_buf(1,m)=0
            numtm_buf(2,m)=0
         end do
         nsegm=m
         ntm=m
      end if
c
      if(idiss.eq.4) then
c
c        association energy of all PDB subunits
c
         nsub=nnsub
         do i=1,nsub
            nsub_su(i)=1
            namsub_su(1,i)=subunit(i)
            numsub_su(1,1,i)=nnum1(i,1)
            numsub_su(2,1,i)=nnum2(i,nnhelix(i))
         end do
c
         m=0
         do i=1,nsub
            do j=1,nnhelix(i)
               m=m+1
               if(m.gt.maxsegm) then
                  write (*,'(''Too many TMH in complex'')') 
                  stop
               end if
               namsegm(m)=subunit(i)
               isegm(1,m)=nnum1(i,j)
               isegm(2,m)=nnum2(i,j)
               isegm_buf(1,m)=0
               isegm_buf(2,m)=0
               namtm(m)=subunit(i)
               numtm(1,m)=nnum1(i,j)
               numtm(2,m)=nnum2(i,j)
               numtm_buf(1,m)=0
               numtm_buf(2,m)=0
            end do
         end do
         nsegm=m
         ntm=m
      end if
c
      if(iprint.ge.2) then
         write (*,'(''  '')')
         do i=1,nsub
            write (*,'(''Subunit '',i4)') i
            do j=1,nsub_su(i)
               write (*,'(a1,2i4)') namsub_su(j,i),
     *           (numsub_su(k,j,i),k=1,2)
            end do
         end do
         do i=1,nsegm
            write (*,'(i4,1x,a1,2i4)') i,namsegm(i),isegm(1,i),
     *        isegm(2,i)
         end do
      end if
c
c     if(ntm.gt.0) then
c        write (*,'(''Transmembrane segments:'')')
c        do i=1,ntm
c           write(*,'(i4,1x,a1,2i4)') i,namtm(i),numtm(1,i),numtm(2,i)
c        end do
c     else
c        write (*,'(''water-soluble domain'')')
c     end if
c
c     2. Read PDB file and renumber sequentially all its residues;
c     eliminate all residues that were not included in definition of
c     "structural subunits"; and redefine borders of subunits, 
c     secondary structures, and transmembrane segments.
c     New PDB file, with sequential residue numbering, will be written 
c     into 'pdbout' (some PDB subunits can merge).
c
      ires_cur=0
      numres0=-999
      namu0='$'
      do i=1,40000
         read (21,'(a)',end=50) line
c
c        use only first structure in case of crystal disorder
c
         if(line(1:4).eq.'ATOM'.and.(line(17:17).eq.' '.or.
     *     line(17:17).eq.'A')) then
            read (line,'(21x,a1,i4)') namu,numres_cur
            do j=1,nsub
               do k=1,nsub_su(j)
                  if(namu.eq.namsub_su(k,j).and.
     *             numres_cur.ge.numsub_su(1,k,j).and.
     *             numres_cur.le.numsub_su(2,k,j)) then
                     if(numres_cur.ne.numres0.or.namu.ne.namu0) then
                        numres0=numres_cur
                        namu0=namu
                        ires_cur=ires_cur+1
c
                        if(numres_cur.eq.numsub_su(1,1,j)) 
     *                    numsub(1,j)=ires_cur
                        if(numres_cur.eq.numsub_su(2,nsub_su(j),j)) 
     *                    numsub(2,j)=ires_cur
c
                        do l=1,nsegm
                           if(namsegm(l).eq.namu) then
                              if(isegm(1,l).eq.numres_cur) 
     *                          isegm_buf(1,l)=ires_cur
                              if(isegm(2,l).eq.numres_cur) 
     *                          isegm_buf(2,l)=ires_cur
                           end if
                        end do
c                                 
                        do l=1,ntm
                           if(namtm(l).eq.namu) then
                              if(numtm(1,l).eq.numres_cur) 
     *                          numtm_buf(1,l)=ires_cur
                              if(numtm(2,l).eq.numres_cur) 
     *                          numtm_buf(2,l)=ires_cur
                           end if
                        end do
                     end if
                     write (22,'(a21,1x,i4,a54)') line(1:21),
     *                 ires_cur,line(27:80)
                  end if
               end do
            end do
         end if
      end do
   50 continue
      nres=ires_cur
c
      do i=1,nsegm
c        write (*,'(2i10)') isegm_buf(1,i),isegm_buf(2,i)
         if(isegm_buf(1,i).eq.0.or.isegm_buf(2,i).eq.0) then
            write (*,'(''Incorrectly defined SS segment'',i3)') i
            stop
            ireturn=1
            close (21)
            close (22)
            return
         end if
         isegm(1,i)=isegm_buf(1,i)
         isegm(2,i)=isegm_buf(2,i)
      end do
c
      do i=1,ntm
         if(numtm_buf(1,i).eq.0.or.numtm_buf(2,i).eq.0) then
            write (*,'(''Incorrectly defined TM segment'',i3)') i
            stop
            ireturn=1
            close (21)
            close (22)
            return
         end if
         numtm(1,i)=numtm_buf(1,i)
         numtm(2,i)=numtm_buf(2,i)
      end do
      write (22,'(''REMARK '',''Structural subunits:'')') 
      do i=1,nsub
         write (22,'(''REMARK '',3i4)') i,(numsub(j,i),j=1,2)
      end do
      write (22,'(''REMARK '',''SS segments:'')') 
      do i=1,nsegm
         write (22,'(''REMARK '',3i4)') i,(isegm(j,i),j=1,2)
      end do
c
      write (22,'(''REMARK '',''TM segments:'')') 
      do i=1,ntm 
         write (22,'(''REMARK '',3i4)') i,(numtm(j,i),j=1,2)
      end do
c
c     close (20)
      close (21)
      close (22)
      return
      end
c
      subroutine active (iprint,pdbout,nsub,numsub,nact,listact)
c     ----------------------------------------------------------
c     Determine active residues for each subunit using structure
c     of the entire complex
c
      parameter (maxsub=25)
c
      include 'template.inc'
      include 'residues.inc'
c
      integer numsub(2,1),nact(1),listact(maxsub,1),
     *  natres_tmpl(maxres)
c
      character*80 line
      character*180 pdbout
c
      data distcut /7.0/
c
c     1. Read template PDB file (now it is continuously
c     renumbered, does not include PDB subunits, and consists
c     only of segments indicated for subunits in .cor file).
c
c     Here, we do not care about atom types, atom order, or 
c     completeness of residues 
c
c     write (*,'(a)') pdbout
      open (22,file=pdbout)
      ires_cur=0
      numres0=-999
      do i=1,99999
         read (22,'(a)',end=10) line
         if(line(1:4).eq.'ATOM') then
            read (line,'(22x,i4,4x,3f8.3)') numres_cur,xc,yc,zc
c           write(*,'(21x,i4,4x,3f8.3)') numres_cur,xc,yc,zc
            if(numres_cur.eq.numres0) then
               iat=iat+1
            else
               numres0=numres_cur
               if(ires_cur.gt.0) natres_tmpl(ires_cur)=iat
               iat=1
               ires_cur=ires_cur+1
            end if
            xyztmpl(1,iat,ires_cur)=xc
            xyztmpl(2,iat,ires_cur)=yc
            xyztmpl(3,iat,ires_cur)=zc
         end if
      end do
   10 continue
      natres_tmpl(ires_cur)=iat
      nres=ires_cur
c
      if(iprint.gt.2) then
         write (*,'(''active:'')')
c        do i=1,nres
c           do j=1,natres_tmpl(i)
c              write (*,'(i4,2x,3f8.3)') i,(xyztmpl(l,j,i),l=1,3)
c           end do
c        end do
      end if
c
c     2. Determine active residues for each subunit,
c     selecting active residues only in TM segments
c
      do i1=1,nsub
         nact(i1)=0
         do j1=numsub(1,i1),numsub(2,i1)
            do i2=1,nsub
               if(i2.ne.i1) then
                  do j2=numsub(1,i2),numsub(2,i2)
                     do k=1,natres_tmpl(j1)
                        do l=1,natres_tmpl(j2)
                           x=xyztmpl(1,k,j1)-xyztmpl(1,l,j2)
                           y=xyztmpl(2,k,j1)-xyztmpl(2,l,j2)
                           z=xyztmpl(3,k,j1)-xyztmpl(3,l,j2)
                           r=sqrt(x*x+y*y+z*z)
                           if(r.lt.distcut) then
                              nact(i1)=nact(i1)+1
                              listact(i1,nact(i1))=j1
                              go to 20
                           end if
                        end do
                     end do
                  end do
               end if
            end do
   20       continue
         end do
c
c        Check that all active residues of subunit 'i1' 
c        belong to transmembrane segments
c
         n1=nact(i1)
         n2=0
         do i=1,n1
            do j=1,ntm
               if(listact(i1,i).ge.numtm(1,j).and.
     *            listact(i1,i).le.numtm(2,j)) then
                  n2=n2+1
                  listact(i1,n2)=listact(i1,i)
                  go to 30
               end if
            end do
   30       continue
         end do
         nact(i1)=n2
         if(iprint.gt.2) write (*,'(''Subunit '',i3,20i4)') 
     *     i1,(listact(i1,j),j=1,nact(i1))
      end do
      close (22)
      return
      end
