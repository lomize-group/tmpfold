c                      residues.inc
c                      ------------
c                 Parameters of residues 
      parameter (max_res=30,maxcnf_res=50,maxat_res=20,maxtors=6)
c
c     max_res   - maximal number of residues in the library
c     maxat_res - maximal number of atoms in residue
c     maxtors   - maximal number of torsion angles in residue 
c     maxcnf_res- maximal number of conformers in residue
c
c                      Residues:
c     ndict      - number of residues in the library
c     namres_full- 4-letter names of residues
c     namres     - 1-letter names of residues
c     prop_hel   - helix propensities, middle positions
c     prop_helc  - helix propensities, C-turn positions
c     prop_ncap  - helix propensities, N-cap positions
c     prop_bet   - beta-sheet propensidies, middle positions 
c     prop_edge  - beta-sheet propensities, edge strand
c     prop_coil  - beta-sheet propensities, edge strand
c     pho_hprot  - water/protein transfer energies, alpha-helix
c     pho_hchx   - water/cyclohexan transfer energies, alpha-helix
c     pho_bet    - water/protein transfer energies, beta-sheet
c     entr_hel   - side-chain conformational entropies in alpha-helix
c     entr_bet   - side-chain conformational entropies in beta-sheet 
c     entr_coil  - side-chain conformational entropies in coil
c
c     natsch     - number of atoms in side-chains
c     isch       - numbers of 1st (CB) and last atoms of side-chain
c     nchi       - number of chi angles (from 0 to 4)
c
c     asa_ref    - reference total ASA from NACCESS
c     asa_hel    - reference total ASA in helix
c     solv_hel   - reference solvation energies of residues in helix
c     solv_bet   - reference solvation energies of residues in beta-sheet
c                  (include those for beta-sheet edge residues later)
c     solv_coil  - reference solvation energies of residues in coil 
c     
c                      Atoms:
c     natres     - number of atoms in residues
c     kndat      - types of atoms
c     namat      - names of atoms
c     qat        - charges of atoms
c     ibonds     - connectivities of atoms
c     bonds      - bond lengths for calc. of atom coordinates
c     valangs    - valent angles for calc. of atom coordinates
c     dihangs    - dihedral angles for calc. of atom coordinates
c
c                  Torsion angles:
c     ntors      - number of torsion angles in residues
c     ktors      - numbers of atoms forming torsion angles
c     ltors      - periodicity of torsion potential
c     etors      - torsion energy barriers
c     namtors    - names of torsion angles
c
c                   Conformers:
c     nconf      - number of conformers in residues
c     xyz_conf   - coordinates of conformers
c     chi_conf   - chi angles 
c
      character*4 namres_full(max_res), namat(maxat_res,max_res),
     *  namtors(maxtors,max_res)
      character*1 namres(2,max_res)
c
      integer isch(2,max_res), natsch(max_res), natres(max_res),
     *  kndat(maxat_res,max_res), ibonds(3,maxat_res,max_res),
     *  ntors(max_res), ktors(2,maxtors,max_res),
     *  ltors(maxtors,max_res),
     *  nconf(max_res), nchi(max_res)
c
      real prop_hel(max_res), prop_helc(max_res), prop_ncap(max_res),
     *  prop_bet(max_res), prop_edge(max_res), prop_coil(max_res),
     *  pho_hprot(max_res), pho_hchx(max_res), pho_bet(max_res),
     *  entr_hel(max_res), entr_bet(max_res), entr_coil(max_res),
     *  asa_ref(max_res),asa_hel(max_res),qat(maxat_res,max_res),
     *  bonds(maxat_res,max_res),valangs(maxat_res,max_res),
     *  dihangs(maxat_res,max_res), 
     *  solv_hel(2,max_res),solv_bet(max_res), solv_coil(max_res),
     *  etors(maxtors,max_res),
     *  xyz_conf(3,maxat_res,maxcnf_res,max_res),
     *  chi_conf(4,maxcnf_res,max_res)
c
      common /residues/ ndict,
     *  prop_hel,prop_helc,prop_ncap,prop_bet,prop_edge,prop_coil,
     *  pho_hprot,pho_hchx,pho_bet,entr_hel,entr_bet,entr_coil,
     *  natsch,isch,nchi,asa_ref,asa_hel,solv_hel,solv_bet,solv_coil,
     *  natres,kndat,qat,ibonds,bonds,valangs,dihangs,
     *  ntors,ktors,ltors,etors,nconf,xyz_conf,chi_conf,
     *  namres_full,namat,namtors,namres
