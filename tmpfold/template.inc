c               template.inc
c               ------------
c           Parameters of template.
c
c     Template residues have continuous numbering starting from 1.
c
      parameter (maxres=2200,maxsegm=50,maxlist_at=100,
     *  maxlist_res=40,maxat_r=15,maxat=maxres*maxat_r)
c
c     maxres   - maximal number of residues in template
c     maxat    - maximal number of atoms in template
c     maxsegm  - maximal number of secondary structures in template
c     maxlist_at - maximal numbers of surrounding atoms in 
c                interaction lists
c     maxlist_res - maximal numbers of surrounding residues 
c     maxat_r  - maximal number of non-hydrogen atoms in residue
c
c     nres     - number of residues in template, including residues
c                between SS segments (all arrays in template.inc 
c                are calculated for 'nres' residues)
c
c     nsegm    - number of SS segments
c     isegm    - 1st and last residue numbers of SS segments
c     indres   - 0 for residues outside core SS segments, and 1 - inside 
c     ntm      - number of transmembrane helices
c     numtm    - numbers of central interfacial residues in TM helices
c
c     ienv     - environmental classes of residues in template
c     fracc    - fractionl accessibilities of residues
c     numref   - reference PDB numbers
c     seq      - amino acid sequence of template
c     iseq     - corresponding numbers of template residues
c                in residue library
c
c     chitmpl  - chi angles in template
c     vect     - translation vector for each position in template
c                to calculate replaced side-chain coordinates
c     rot      - rotational transformation matrix for each position
c     v1       - translation vector for the center of mass of four
c                backbone atoms in rothel.lib coordinate stystem
c     xyztmpl  - template coordinates, all nonhydrogen atoms
c     ienvat   - environments of template atoms (currently, mark
c                as '1' water-accessible atoms) 
c
c     listres  - list of residues for calculations of ASA
c                (i and all surrounding residues)
c     nlistres - number of residues in this list
c               
c     listsur  - list of residues surrounding i,  for calculations 
c               of Eij, excluding own helix
c     nlistsur - number of residues in this list
c
c     indact =0 - not active residue in template
c            =1 - active residue
c
      character*4 seq(maxres)
c
      integer nsegm,isegm(2,maxsegm),indres(maxres),ienv(maxres),
     *  numref(maxres),iseq(maxres),ienvat(maxat_r,maxres),
     *  listres(maxlist_res,maxres),listsur(maxlist_res,maxres),
     *  nlistres(maxres),nlistsur(maxres),numtm(2,maxsegm),
     *  indact(maxres)
c
      real fracc(maxres),chitmpl(4,maxres),v1(3),
     * vect(3,maxres),rot(3,3,maxres), xyztmpl(3,maxat_r,maxres),
     * xmod(3,6,maxres)
c
      common /templ/ nres,nsegm,isegm,ntm,numtm,indres,ienv,fracc,
     *  numref,iseq,chitmpl,vect,rot,v1,xyztmpl,xmod,ienvat,
     *  nlistres,listres,nlistsur,listsur,indact,seq
